import pandas
import pandas as pd
import unidecode
import random

class QG:

    def __init__(self,path):

        print("Initializing QG")
        self.path=path
        self.possibilities=self.get_possibles()


    def generate_questions(self, how_many):
        """
        Generate random questions
        :param how_many: number of question to be generated
        :return: list of strings
        """
        drinks, alcoholic, categories, ingredients=self.possibilities

        # questions about having a drink
        DQ=[
            "I would like a %s",
            "Make me a %s",
            "Can I have a %s",
        ]

        #excluding some ingredients
        Cases=[
            " without %s",
            " but no %s",
            " without %s and %s",
            #" but no %s and no %s",
            #" without %s with $s instead",
        ]

        #question about categories of drinks
        Categories=[
            "What %s do you have",
            "Show me the %s you have",
            "You have any %s",
        ]

        questions=[]


        basic_case=how_many//3

        for i in range(basic_case*2):
            dq=random.choice(DQ)
            drink=random.choice(drinks)
            questions.append(dq.replace("%s",drink))

        for i in range(basic_case):
            case=questions[i]
            case+=random.choice(Cases)

            for j in range(case.count("%s")):
                case=case.replace("%s",random.choice(ingredients),1)

            questions[i]=case

        for i in range(basic_case):
            cat=random.choice(Categories)
            cat=cat.replace("%s",random.choice(categories))
            questions.append(cat)

        random.shuffle(questions)
        return questions

    def get_possibles(self):
        """
        Returns all the possible objects in the database
        :return:
        """
        # read recepies dataset
        df=self.read_recepies()

        # get all the objects as sets
        drinks=list(set(df['strDrink']))
        alcoholic=list(set(df['strAlcoholic']))
        categories=list(set(df['strCategory']))

        ingredients=[]

        for i in range(1,16):
            ingredients+=df[f"strIngredient{i}"]

        ingredients=[elem for elem in ingredients if not pd.isna(elem)]
        ingredients=list(set(ingredients))


        return drinks,alcoholic,categories,ingredients

    def read_recepies(self):
        """
        Read the recepies file and apply some changes to the dataset before passing it as a list
        :return:
        """

        # read the file
        df = pd.read_csv(self.path.recepies)

        # make a list of columns to drop  and drop them
        to_drop = ['strDrinkThumb', 'strGlass',
                   'strVideo', 'strInstructions',
                   'idDrink', 'strIBA',
                   'dateModified']

        # drop all the measures
        for i in range(1, 16):
            to_drop.append(f"strMeasure{i}")

        df = df.drop(columns=to_drop)

        # drop rows where the alcoholic value is other than non/alcoholic
        df = df[df['strAlcoholic'].isin(['Alcoholic', 'Non alcoholic'])]

        # remove some category
        df = df[df['strCategory'] != 'Other/Unknown']
        df = df[df['strCategory'] != 'Cocoa']
        df = df[df['strCategory'] != 'Milk / Float / Shake']
        df = df[df['strCategory'] != 'Coffee / Tea']

        # define a mapping between categories names
        mapping = {'Ordinary Drink': "Drink",
                   'Homemade Liqueur': 'Liqueur',
                   'Punch / Party Drink': "Drink",
                   'Soft Drink / Soda': "Soda", }
        # and substitute
        df = df.replace(mapping)

        # remove quotes from
        replaced = df['strDrink'].str.replace("[\"\',.)(&#]", '')
        # replace accented characted
        replaced = replaced.apply(lambda x: unidecode.unidecode(x) if isinstance(x, str) else x)
        # remove any drink which starts with a letter
        df['strDrink'] = replaced.apply(lambda x: "Unk" if x[0].isdigit() else x)

        # same thing for ingredients
        for i in range(1, 16):
            replaced = df[f"strIngredient{i}"].apply(lambda x: unidecode.unidecode(x) if isinstance(x, str) else x)
            df[f"strIngredient{i}"] = replaced.apply(lambda x: "Unk" if not pd.isna(x) and x[0].isdigit() else x)

        # return a listed version of the dataframe
        return df.to_dict(orient="list")