from Path import Path
from Speech.answering import Asnwering
from Test.QuestionGeneration import QG
from Speech.processing import StanfordNLP
from Speech.recognition import recognize_audio
from prolog_query import CocktailProlog


N=90

# initialize path class
paths=Path()

# convert audio
audio=recognize_audio(paths.cocktail_wav1)

# initialize prolog class
prolog=CocktailProlog(paths,False)

# initialize question generator calss
qg=QG(paths)

# generate N questions
questions=qg.generate_questions(N)

# initialize standfordNLP and answering
nlp = StanfordNLP(paths)
answering=Asnwering(paths)


# for every generated question
for q in questions:
    #analyze the sentence
    intent=nlp.get_intent(q)
    #if not understood pass
    if len(intent)==0: continue
    # get the possible answers
    answer=prolog.answer_question(intent)
    #speak them
    answering.answer(answer)

#close the standfordcore
nlp.close()

