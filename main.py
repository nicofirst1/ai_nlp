from Path import Path
from Speech.answering import Asnwering
from Speech.processing import StanfordNLP
from Speech.recognition import RecordListener
from prolog_query import CocktailProlog



# initialize path class
paths=Path()


# initialize prolog class
prolog=CocktailProlog(paths,False)

# initialize standfordNLP and answering
nlp = StanfordNLP(paths)
answering=Asnwering(paths)
nlp.get_intent("this is a test intent")

input=""


to_say="Welcome to the AI bar. To record your voice press r. What would you like to drink?"

answering.say(to_say)

speech_recognizer=RecordListener(nlp,answering,prolog)
speech_recognizer.start()

