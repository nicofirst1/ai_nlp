
Direct questions [DQ]:

- I would like a Cocktail
- Make me a Cocktail
- Can I have a Cocktail 

Exclusion :

- DQ, without Ingredient1
- DQ, without Ingredient1 and Ingredient2 and ... IngredientN
- DQ, but no Ingredient

Listing:

- What Category do you have?
- Can I have something Non/Alcoholic?
