:-style_check(-discontiguous).

quantity(champagne,10).
quantity(whiskey,10).
quantity(tiamaria,10).
quantity(agavesyrup,5).
quantity(fruitjuice,8).
quantity(vodka,9).
quantity(ice,8).
quantity(pisco,1).
quantity(mint,3).
quantity(hotchocolate,6).
quantity(greencremedementhe,4).
quantity(lemonjuice,6).
quantity(aperol,8).
quantity(port,5).
quantity(apfelkorn,8).
quantity(blackberrybrandy,7).
quantity(guavajuice,3).
quantity(brandy,8).
quantity(powderedsugar,0).
quantity(yukonjack,4).
quantity(carrot,2).
quantity(apple,0).
quantity(sambuca,1).
quantity(whitecremedementhe,3).
quantity(appleschnapps,2).
quantity(grapejuice,2).
quantity(vermouth,8).
quantity(fennelseeds,10).
quantity(pinacoladamix,3).
quantity(apricotbrandy,2).
quantity(caramelcoloring,3).
quantity(coffeebrandy,4).
quantity(cranberryjuice,2).
quantity(anisette,2).
quantity(triplesec,2).
quantity(vanillavodka,9).
quantity(strawberryliqueur,4).
quantity(wormwood,0).
quantity(cherryliqueur,6).
quantity(sugar,2).
quantity(cloves,7).
quantity(tequila,6).
quantity(anis,7).
quantity(kahlua,0).
quantity(ricard,10).
quantity(curacao,8).
quantity(ryewhiskey,8).
quantity(orangepeel,9).
quantity(lime,0).
quantity(goldtequila,4).
quantity(hotdamn,8).
quantity(carbonatedwater,8).
quantity(orangecuracao,10).
quantity(jackdaniels,6).
quantity(ginger,9).
quantity(jello,4).
quantity(maraschinocherry,4).
quantity(celerysalt,4).
quantity(cherrybrandy,7).
quantity(licoriceroot,8).
quantity(tabascosauce,1).
quantity(sugarsyrup,9).
quantity(erincream,6).
quantity(coriander,3).
quantity(pineapple,1).
quantity(bacardilimon,2).
quantity(grainalcohol,0).
quantity(jagermeister,3).
quantity(kirschwasser,10).
quantity(frangelico,5).
quantity(pinklemonade,3).
quantity(freshlemonjuice,6).
quantity(crownroyal,7).
quantity(bitterlemon,6).
quantity(goldschlager,6).
quantity(rootbeer,1).
quantity(zima,7).
quantity(chambordraspberryliqueur,1).
quantity(cranberries,6).
quantity(limejuice,8).
quantity(half-and-half,7).
quantity(foodcoloring,9).
quantity(limejuicecordial,2).
quantity(absolutpeppar,9).
quantity(absolutkurant,8).
quantity(chocolatesyrup,9).
quantity(tropicana,10).
quantity(passionfruitsyrup,2).
quantity(olive,0).
quantity(blackcurrantsquash,7).
quantity(cointreau,6).
quantity(gingerale,5).
quantity(brownsugar,5).
quantity(anise,0).
quantity(orangejuice,6).
quantity(campari,8).
quantity(blacksambuca,3).
quantity(oreocookie,10).
quantity(cremedemure,4).
quantity(allspice,2).
quantity(pineapplejuice,0).
quantity(dryvermouth,8).
quantity(stgermain,4).
quantity(ale,3).
quantity(fruitpunch,8).
quantity(clubsoda,0).
quantity(blendedwhiskey,5).
quantity(coffeeliqueur,10).
quantity(sarsaparilla,2).
quantity(honey,9).
quantity(sherbet,4).
quantity(lavender,9).
quantity(salt,7).
quantity(grapefruitjuice,0).
quantity(jimbeam,0).
quantity(johnniewalker,5).
quantity(coconutmilk,4).
quantity(absinthe,7).
quantity(cherry,6).
quantity(water,2).
quantity(drambuie,0).
quantity(blackcurrantcordial,7).
quantity(blueberryschnapps,5).
quantity(lightcream,9).
quantity(banana,4).
quantity(peppermintschnapps,3).
quantity(chocolateliqueur,2).
quantity(limevodka,8).
quantity(strawberryschnapps,10).
quantity(applebrandy,9).
quantity(kummel,3).
quantity(condensedmilk,10).
quantity(whiterum,4).
quantity(pepsicola,2).
quantity(cranberryvodka,9).
quantity(ouzo,10).
quantity(prosecco,5).
quantity(milk,1).
quantity(sweetvermouth,3).
quantity(absolutvodka,5).
quantity(midorimelonliqueur,3).
quantity(worcestershiresauce,10).
quantity(peachnectar,4).
quantity(peachschnapps,5).
quantity(vanillaextract,4).
quantity(irishcream,0).
quantity(grenadine,4).
quantity(orange,6).
quantity(cherries,2).
quantity(applejuice,1).
quantity(coffee,3).
quantity(mountaindew,7).
quantity(lightrum,4).
quantity(pisangambon,5).
quantity(apricot,10).
quantity(almondflavoring,4).
quantity(applecider,3).
quantity(unk,0).
quantity(bitters,2).
quantity(darkrum,4).
quantity(irishwhiskey,2).
quantity(galliano,7).
quantity(sodawater,1).
quantity(coconutsyrup,3).
quantity(beer,2).
quantity(grandmarnier,1).
quantity(carbonatedsoftdrink,7).
quantity(vanillaice-cream,7).
quantity(passionfruitjuice,2).
quantity(almond,2).
quantity(maui,5).
quantity(demerarasugar,10).
quantity(greenchartreuse,0).
quantity(peppermintextract,10).
quantity(gin,5).
quantity(corona,5).
quantity(maliburum,9).
quantity(peachvodka,10).
quantity(raspberryliqueur,7).
quantity(angelicaroot,0).
quantity(freshlimejuice,7).
quantity(eggyolk,6).
quantity(maraschinoliqueur,9).
quantity(strawberries,1).
quantity(cremedecassis,8).
quantity(benedictine,1).
quantity(schweppesrusschian,4).
quantity(lilletblanc,4).
quantity(orgeatsyrup,0).
quantity(sherry,0).
quantity(sprite,1).
quantity(baileyirishcream,10).
quantity(marjoramleaves,10).
quantity(drpepper,9).
quantity(lemon-limesoda,5).
quantity(peachtreeschnapps,7).
quantity(amaretto,8).
quantity(bluecuracao,6).
quantity(cognac,7).
quantity(spicedrum,4).
quantity(lemon,5).
quantity(lemonade,2).
quantity(olivebrine,4).
quantity(wildturkey,8).
quantity(bourbon,7).
quantity(eggwhite,6).
quantity(coconutrum,2).
quantity(guinnessstout,6).
quantity(peachbitters,7).
quantity(kool-aid,5).
quantity(creamofcoconut,7).
quantity(coconutliqueur,0).
quantity(sloegin,2).
quantity(candy,8).
quantity(icedtea,7).
quantity(lemonpeel,1).
quantity(applejack,5).
quantity(whippedcream,10).
quantity(cherryheering,3).
quantity(sweetandsour,0).
quantity(butterscotchschnapps,6).
quantity(peachbrandy,2).
quantity(kiwiliqueur,3).
quantity(maplesyrup,3).
quantity(darkcremedecacao,10).
quantity(bananaliqueur,6).
quantity(surge,1).
quantity(tea,8).
quantity(yellowchartreuse,1).
quantity(cremedecacao,3).
quantity(rumpleminze,0).
quantity(vanilla,0).
quantity(raspberrysyrup,9).
quantity(absolutcitron,6).
quantity(limepeel,4).
quantity(melonliqueur,7).
quantity(peychaudbitters,4).
quantity(cachaca,2).
quantity(egg,0).
quantity(cinnamon,7).
quantity(glycerine,0).
quantity(anejorum,2).
quantity(cream,9).
quantity(rum,6).
quantity(dubonnetrouge,3).
quantity(southerncomfort,6).
quantity(orangebitters,8).
quantity(grapesoda,3).
quantity(cider,2).
quantity(tomatojuice,4).
quantity(sourmix,0).
quantity(gingerbeer,5).
quantity(everclear,0).
quantity(pepper,1).
quantity(heavycream,9).
quantity(daiquirimix,10).
quantity(lemonvodka,1).
quantity(orangespiral,10).
quantity(chocolateice-cream,7).
quantity(cremedebanane,1).
quantity(coca-cola,6).
quantity(sirupofroses,7).
quantity(scotch,3).
quantity(wine,8).
quantity(advocaat,0).
quantity(firewater,9).
quantity(cornsyrup,5).
quantity(fresca,10).
quantity(pineapplesyrup,5).
quantity(tonicwater,3).
quantity(lager,10).
quantity(baileysirishcream,2).
quantity(nutmeg,3).
quantity(fruit,9).
quantity(angosturabitters,5).
quantity(redwine,4).
alcoholic(adayatthebeach).
category(adayatthebeach,drink).
ingredients(adayatthebeach,[coconutrum, amaretto, orangejuice, grenadine]).
drink(adayatthebeach).


alcoholic(afurlongtoolate).
category(afurlongtoolate,drink).
ingredients(afurlongtoolate,[lightrum, gingerbeer, lemonpeel]).
drink(afurlongtoolate).


alcoholic(agilligansisland).
category(agilligansisland,cocktail).
ingredients(agilligansisland,[vodka, peachschnapps, orangejuice, cranberryjuice]).
drink(agilligansisland).


alcoholic(anightinoldmandalay).
category(anightinoldmandalay,drink).
ingredients(anightinoldmandalay,[lightrum, anejorum, orangejuice, lemonjuice, gingerale, lemonpeel]).
drink(anightinoldmandalay).


alcoholic(asplashofnash).
category(asplashofnash,shot).
ingredients(asplashofnash,[cranberryjuice, sodawater, midorimelonliqueur, cremedebanane]).
drink(asplashofnash).


alcoholic(atrueamarettosour).
category(atrueamarettosour,cocktail).
ingredients(atrueamarettosour,[amaretto, lemon, ice, maraschinocherry]).
drink(atrueamarettosour).


alcoholic(amidsummernightdream).
category(amidsummernightdream,drink).
ingredients(amidsummernightdream,[vodka, kirschwasser, strawberryliqueur, strawberries, schweppesrusschian]).
drink(amidsummernightdream).


alcoholic(aj).
category(aj,drink).
ingredients(aj,[applejack, grapefruitjuice]).
drink(aj).


alcoholic(admafterdinnermint).
category(admafterdinnermint,cocktail).
ingredients(admafterdinnermint,[whitecremedementhe, southerncomfort, vodka, hotchocolate]).
drink(admafterdinnermint).


alcoholic(a1).
category(a1,cocktail).
ingredients(a1,[gin, grandmarnier, lemonjuice, grenadine]).
drink(a1).


alcoholic(abc).
category(abc,shot).
ingredients(abc,[amaretto, baileyirishcream, cognac]).
drink(abc).


alcoholic(acid).
category(acid,shot).
ingredients(acid,[unk, wildturkey]).
drink(acid).


alcoholic(att).
category(att,drink).
ingredients(att,[absolutvodka, gin, tonicwater]).
drink(att).


alcoholic(abbeycocktail).
category(abbeycocktail,drink).
ingredients(abbeycocktail,[gin, orangebitters, orange, cherry]).
drink(abbeycocktail).


alcoholic(abbeymartini).
category(abbeymartini,cocktail).
ingredients(abbeymartini,[gin, sweetvermouth, orangejuice, angosturabitters]).
drink(abbeymartini).


alcoholic(abilene).
category(abilene,drink).
ingredients(abilene,[darkrum, peachnectar, orangejuice]).
drink(abilene).


alcoholic(absinthe2).
category(absinthe2,liqueur).
ingredients(absinthe2,[vodka, sugar, anise, licoriceroot, wormwood]).
drink(absinthe2).


alcoholic(absolutsex).
category(absolutsex,shot).
ingredients(absolutsex,[absolutkurant, midorimelonliqueur, cranberryjuice, sprite]).
drink(absolutsex).


alcoholic(absolutstress2).
category(absolutstress2,drink).
ingredients(absolutstress2,[absolutvodka, peachschnapps, coconutliqueur, cranberryjuice, pineapplejuice]).
drink(absolutstress2).


alcoholic(absolutsummertime).
category(absolutsummertime,cocktail).
ingredients(absolutsummertime,[absolutcitron, sweetandsour, sprite, sodawater, lemon]).
drink(absolutsummertime).


alcoholic(absolutelycranberrysmash).
category(absolutelycranberrysmash,drink).
ingredients(absolutelycranberrysmash,[absolutvodka, cranberryjuice, gingerale, ice]).
drink(absolutelycranberrysmash).


alcoholic(absolutelyfabulous).
category(absolutelyfabulous,cocktail).
ingredients(absolutelyfabulous,[vodka, cranberryjuice, champagne]).
drink(absolutelyfabulous).


alcoholic(absolutlyscrewedup).
category(absolutlyscrewedup,cocktail).
ingredients(absolutlyscrewedup,[absolutcitron, orangejuice, triplesec, gingerale]).
drink(absolutlyscrewedup).


alcoholic(acapulco).
category(acapulco,drink).
ingredients(acapulco,[lightrum, triplesec, limejuice, sugar, eggwhite, mint]).
drink(acapulco).


alcoholic(ace).
category(ace,cocktail).
ingredients(ace,[gin, grenadine, heavycream, milk, eggwhite]).
drink(ace).


alcoholic(adam).
category(adam,drink).
ingredients(adam,[darkrum, lemonjuice, grenadine]).
drink(adam).


alcoholic(adameve).
category(adameve,cocktail).
ingredients(adameve,[gin, cognac, cremedecassis, freshlemonjuice]).
drink(adameve).


alcoholic(adambomb).
category(adambomb,drink).
ingredients(adambomb,[rum, vodka, tequila, triplesec, fruit, ice, salt, fruitjuice]).
drink(adambomb).


alcoholic(adamsunrise).
category(adamsunrise,drink).
ingredients(adamsunrise,[vodka, lemonade, water, sugar]).
drink(adamsunrise).


alcoholic(addington).
category(addington,cocktail).
ingredients(addington,[sweetvermouth, dryvermouth, sodawater]).
drink(addington).


alcoholic(addison).
category(addison,cocktail).
ingredients(addison,[gin, vermouth]).
drink(addison).


alcoholic(addisonspecial).
category(addisonspecial,cocktail).
ingredients(addisonspecial,[vodka, grenadine, orangejuice]).
drink(addisonspecial).


alcoholic(adiosamigoscocktail).
category(adiosamigoscocktail,cocktail).
ingredients(adiosamigoscocktail,[rum, dryvermouth, cognac, gin, freshlimejuice, sugarsyrup, water]).
drink(adiosamigoscocktail).


alcoholic(adoniscocktail).
category(adoniscocktail,drink).
ingredients(adoniscocktail,[sweetvermouth, sherry, orangebitters]).
drink(adoniscocktail).


alcoholic(affair).
category(affair,drink).
ingredients(affair,[strawberryschnapps, orangejuice, cranberryjuice, clubsoda]).
drink(affair).


alcoholic(affinity).
category(affinity,drink).
ingredients(affinity,[scotch, sweetvermouth, dryvermouth, orangebitters]).
drink(affinity).


alcoholic(afterdinnercocktail).
category(afterdinnercocktail,drink).
ingredients(afterdinnercocktail,[apricotbrandy, triplesec, lime, lime]).
drink(afterdinnercocktail).


alcoholic(afterfive).
category(afterfive,shot).
ingredients(afterfive,[peppermintschnapps, kahlua, baileyirishcream]).
drink(afterfive).


alcoholic(aftersuppercocktail).
category(aftersuppercocktail,drink).
ingredients(aftersuppercocktail,[triplesec, apricotbrandy, lemonjuice]).
drink(aftersuppercocktail).


alcoholic(aftersex).
category(aftersex,drink).
ingredients(aftersex,[vodka, cremedebanane, orangejuice]).
drink(aftersex).


non_alcoholic(afterglow).
category(afterglow,cocktail).
ingredients(afterglow,[grenadine, orangejuice, pineapplejuice]).
drink(afterglow).


alcoholic(alabamaslammer).
category(alabamaslammer,drink).
ingredients(alabamaslammer,[southerncomfort, amaretto, sloegin, lemonjuice]).
drink(alabamaslammer).


alcoholic(alaskacocktail).
category(alaskacocktail,drink).
ingredients(alaskacocktail,[orangebitters, gin, yellowchartreuse, lemonpeel]).
drink(alaskacocktail).


alcoholic(alexander).
category(alexander,drink).
ingredients(alexander,[gin, cremedecacao, lightcream, nutmeg]).
drink(alexander).


alcoholic(alfiecocktail).
category(alfiecocktail,drink).
ingredients(alfiecocktail,[lemonvodka, triplesec, pineapplejuice]).
drink(alfiecocktail).


alcoholic(algonquin).
category(algonquin,drink).
ingredients(algonquin,[blendedwhiskey, dryvermouth, pineapplejuice]).
drink(algonquin).


non_alcoholic(alicecocktail).
category(alicecocktail,cocktail).
ingredients(alicecocktail,[grenadine, orangejuice, pineapplejuice, cream]).
drink(alicecocktail).


alcoholic(aliceinwonderland).
category(aliceinwonderland,drink).
ingredients(aliceinwonderland,[amaretto, grandmarnier, southerncomfort]).
drink(aliceinwonderland).


alcoholic(allegheny).
category(allegheny,drink).
ingredients(allegheny,[dryvermouth, bourbon, blackberrybrandy, lemonjuice, lemonpeel]).
drink(allegheny).


alcoholic(alliescocktail).
category(alliescocktail,drink).
ingredients(alliescocktail,[dryvermouth, gin, kummel]).
drink(alliescocktail).


alcoholic(almeria).
category(almeria,drink).
ingredients(almeria,[darkrum, kahlua, eggwhite]).
drink(almeria).


alcoholic(almondchocolatecoffee).
category(almondchocolatecoffee,drink).
ingredients(almondchocolatecoffee,[amaretto, darkcremedecacao, coffee]).
drink(almondchocolatecoffee).


alcoholic(almondjoy).
category(almondjoy,drink).
ingredients(almondjoy,[amaretto, cremedecacao, lightcream]).
drink(almondjoy).


non_alcoholic(alohafruitpunch).
category(alohafruitpunch,drink).
ingredients(alohafruitpunch,[water, ginger, guavajuice, lemonjuice, pineapple, sugar, pineapplejuice]).
drink(alohafruitpunch).


alcoholic(amarettoandcream).
category(amarettoandcream,drink).
ingredients(amarettoandcream,[amaretto, lightcream]).
drink(amarettoandcream).


alcoholic(amarettoliqueur).
category(amarettoliqueur,liqueur).
ingredients(amarettoliqueur,[sugar, water, apricot, almondflavoring, grainalcohol, water, brandy, foodcoloring, foodcoloring, foodcoloring, glycerine]).
drink(amarettoliqueur).


alcoholic(amarettomist).
category(amarettomist,drink).
ingredients(amarettomist,[amaretto, lime]).
drink(amarettomist).


alcoholic(amarettorose).
category(amarettorose,drink).
ingredients(amarettorose,[amaretto, limejuice, clubsoda]).
drink(amarettorose).


alcoholic(amarettoshake).
category(amarettoshake,drink).
ingredients(amarettoshake,[chocolateice-cream, brandy, amaretto]).
drink(amarettoshake).


alcoholic(amarettosour).
category(amarettosour,drink).
ingredients(amarettosour,[amaretto, sourmix]).
drink(amarettosour).


alcoholic(amarettostinger).
category(amarettostinger,drink).
ingredients(amarettostinger,[amaretto, whitecremedementhe]).
drink(amarettostinger).


alcoholic(amarettosunrise).
category(amarettosunrise,drink).
ingredients(amarettosunrise,[amaretto, orangejuice, grenadine]).
drink(amarettosunrise).


alcoholic(amarettosunset).
category(amarettosunset,drink).
ingredients(amarettosunset,[triplesec, amaretto, cider, ice]).
drink(amarettosunset).


alcoholic(amarettosweetsour).
category(amarettosweetsour,drink).
ingredients(amarettosweetsour,[amaretto, sweetandsour, midorimelonliqueur, pineapplejuice]).
drink(amarettosweetsour).


alcoholic(amarettotea).
category(amarettotea,drink).
ingredients(amarettotea,[tea, amaretto, whippedcream]).
drink(amarettotea).


alcoholic(americano).
category(americano,drink).
ingredients(americano,[campari, sweetvermouth, lemonpeel, orangepeel]).
drink(americano).


alcoholic(angelface).
category(angelface,drink).
ingredients(angelface,[apricotbrandy, applebrandy, gin]).
drink(angelface).


alcoholic(angelicaliqueur).
category(angelicaliqueur,liqueur).
ingredients(angelicaliqueur,[angelicaroot, almond, allspice, cinnamon, anise, coriander, marjoramleaves, vodka, sugar, water, foodcoloring, foodcoloring]).
drink(angelicaliqueur).


alcoholic(applegrande).
category(applegrande,drink).
ingredients(applegrande,[tequila, applecider]).
drink(applegrande).


non_alcoholic(applekarate).
category(applekarate,cocktail).
ingredients(applekarate,[applejuice, carrot]).
drink(applekarate).


alcoholic(appleslammer).
category(appleslammer,shot).
ingredients(appleslammer,[unk, appleschnapps]).
drink(appleslammer).


alcoholic(applecar).
category(applecar,drink).
ingredients(applecar,[applejack, triplesec, lemonjuice]).
drink(applecar).


alcoholic(applejack).
category(applejack,cocktail).
ingredients(applejack,[jackdaniels, midorimelonliqueur, sourmix]).
drink(applejack).


alcoholic(apricotlady).
category(apricotlady,drink).
ingredients(apricotlady,[lightrum, apricotbrandy, triplesec, lemonjuice, eggwhite, orange]).
drink(apricotlady).


alcoholic(apricotpunch).
category(apricotpunch,drink).
ingredients(apricotpunch,[apricotbrandy, champagne, vodka, unk, orangejuice]).
drink(apricotpunch).


alcoholic(archbishop).
category(archbishop,drink).
ingredients(archbishop,[gin, wine, benedictine, lime]).
drink(archbishop).


alcoholic(arcticfish).
category(arcticfish,drink).
ingredients(arcticfish,[vodka, grapesoda, orangejuice, ice, candy]).
drink(arcticfish).


alcoholic(arcticmouthwash).
category(arcticmouthwash,drink).
ingredients(arcticmouthwash,[maui, mountaindew, ice]).
drink(arcticmouthwash).


alcoholic(arisemylove).
category(arisemylove,drink).
ingredients(arisemylove,[champagne, greencremedementhe]).
drink(arisemylove).


alcoholic(arizonaantifreeze).
category(arizonaantifreeze,shot).
ingredients(arizonaantifreeze,[vodka, midorimelonliqueur, sweetandsour]).
drink(arizonaantifreeze).


alcoholic(arizonastingers).
category(arizonastingers,cocktail).
ingredients(arizonastingers,[absolutvodka, icedtea]).
drink(arizonastingers).


alcoholic(arizonatwister).
category(arizonatwister,cocktail).
ingredients(arizonatwister,[vodka, maliburum, goldtequila, orangejuice, pineapplejuice, creamofcoconut, grenadine, ice, pineapple]).
drink(arizonatwister).


alcoholic(armyspecial).
category(armyspecial,cocktail).
ingredients(armyspecial,[vodka, gin, limejuicecordial, ice]).
drink(armyspecial).


alcoholic(arthurtompkins).
category(arthurtompkins,drink).
ingredients(arthurtompkins,[gin, grandmarnier, lemonjuice, lemonpeel]).
drink(arthurtompkins).


alcoholic(artillery).
category(artillery,drink).
ingredients(artillery,[sweetvermouth, gin, bitters]).
drink(artillery).


alcoholic(artillerypunch).
category(artillerypunch,drink).
ingredients(artillerypunch,[tea, ryewhiskey, redwine, rum, brandy, benedictine, orangejuice, lemonjuice]).
drink(artillerypunch).


alcoholic(atlanticsun).
category(atlanticsun,drink).
ingredients(atlanticsun,[vodka, southerncomfort, passionfruitsyrup, sweetandsour, clubsoda]).
drink(atlanticsun).


alcoholic(atomiclokade).
category(atomiclokade,drink).
ingredients(atomiclokade,[lemonade, vodka, bluecuracao, triplesec, sugar, ice]).
drink(atomiclokade).


alcoholic(auburnheadbanger).
category(auburnheadbanger,shot).
ingredients(auburnheadbanger,[jagermeister, goldschlager]).
drink(auburnheadbanger).


alcoholic(autodafe).
category(autodafe,drink).
ingredients(autodafe,[vodka, limejuice, sodawater]).
drink(autodafe).


alcoholic(avalon).
category(avalon,drink).
ingredients(avalon,[vodka, pisangambon, applejuice, lemonjuice, lemonade]).
drink(avalon).


alcoholic(aviation).
category(aviation,cocktail).
ingredients(aviation,[gin, lemonjuice, maraschinoliqueur]).
drink(aviation).


alcoholic(aztecpunch).
category(aztecpunch,drink).
ingredients(aztecpunch,[lemonade, vodka, rum, gingerale]).
drink(aztecpunch).


alcoholic(b-52).
category(b-52,shot).
ingredients(b-52,[baileyirishcream, grandmarnier, kahlua]).
drink(b-52).


alcoholic(b-53).
category(b-53,shot).
ingredients(b-53,[kahlua, sambuca, grandmarnier]).
drink(b-53).


alcoholic(babyguinness).
category(babyguinness,shot).
ingredients(babyguinness,[kahlua, baileyirishcream]).
drink(babyguinness).


alcoholic(bacardicocktail).
category(bacardicocktail,drink).
ingredients(bacardicocktail,[lightrum, limejuice, sugarsyrup, grenadine]).
drink(bacardicocktail).


alcoholic(baileydreamshake).
category(baileydreamshake,soda).
ingredients(baileydreamshake,[baileyirishcream, vanillaice-cream, cream]).
drink(baileydreamshake).


alcoholic(balmoral).
category(balmoral,drink).
ingredients(balmoral,[scotch, sweetvermouth, dryvermouth, bitters]).
drink(balmoral).


alcoholic(bananadaiquiri).
category(bananadaiquiri,drink).
ingredients(bananadaiquiri,[lightrum, triplesec, banana, limejuice, sugar, cherry]).
drink(bananadaiquiri).


alcoholic(barracuda).
category(barracuda,drink).
ingredients(barracuda,[rum, galliano, pineapplejuice, limejuice, prosecco]).
drink(barracuda).


alcoholic(belgianblue).
category(belgianblue,soda).
ingredients(belgianblue,[vodka, coconutliqueur, bluecuracao, sprite]).
drink(belgianblue).


alcoholic(bellini).
category(bellini,drink).
ingredients(bellini,[champagne, peachschnapps]).
drink(bellini).


alcoholic(bellinimartini).
category(bellinimartini,drink).
ingredients(bellinimartini,[ice, vodka, peachnectar, peachschnapps, lemonpeel]).
drink(bellinimartini).


alcoholic(bermudahighball).
category(bermudahighball,drink).
ingredients(bermudahighball,[brandy, gin, dryvermouth, carbonatedwater, lemonpeel]).
drink(bermudahighball).


alcoholic(berrydeadly).
category(berrydeadly,drink).
ingredients(berrydeadly,[everclear, wine, orangejuice, kool-aid]).
drink(berrydeadly).


alcoholic(betweenthesheets).
category(betweenthesheets,drink).
ingredients(betweenthesheets,[brandy, lightrum, triplesec, lemonjuice]).
drink(betweenthesheets).


alcoholic(bigred).
category(bigred,shot).
ingredients(bigred,[irishcream, goldschlager]).
drink(bigred).


alcoholic(blacktan).
category(blacktan,beer).
ingredients(blacktan,[ale, guinnessstout]).
drink(blacktan).


alcoholic(blackrussian).
category(blackrussian,drink).
ingredients(blackrussian,[coffeeliqueur, vodka]).
drink(blackrussian).


alcoholic(blackandbrown).
category(blackandbrown,beer).
ingredients(blackandbrown,[guinnessstout, rootbeer]).
drink(blackandbrown).


alcoholic(blackthorn).
category(blackthorn,drink).
ingredients(blackthorn,[sweetvermouth, sloegin, lemonpeel]).
drink(blackthorn).


alcoholic(bleedingsurgeon).
category(bleedingsurgeon,soda).
ingredients(bleedingsurgeon,[darkrum, orange, surge, cranberryjuice]).
drink(bleedingsurgeon).


alcoholic(bloodymaria).
category(bloodymaria,drink).
ingredients(bloodymaria,[tequila, tomatojuice, lemonjuice, tabascosauce, celerysalt, lemon]).
drink(bloodymaria).


alcoholic(bloodymary).
category(bloodymary,drink).
ingredients(bloodymary,[vodka, tomatojuice, lemonjuice, worcestershiresauce, tabascosauce, lime]).
drink(bloodymary).


alcoholic(bluelagoon).
category(bluelagoon,drink).
ingredients(bluelagoon,[vodka, bluecuracao, lemonade, cherry]).
drink(bluelagoon).


alcoholic(bluemargarita).
category(bluemargarita,drink).
ingredients(bluemargarita,[tequila, bluecuracao, limejuice, salt]).
drink(bluemargarita).


alcoholic(bluemountain).
category(bluemountain,drink).
ingredients(bluemountain,[anejorum, tiamaria, vodka, orangejuice, lemonjuice]).
drink(bluemountain).


alcoholic(bluebird).
category(bluebird,drink).
ingredients(bluebird,[gin, triplesec, bluecuracao, bitters, maraschinocherry, lemonpeel]).
drink(bluebird).


alcoholic(bobmarley).
category(bobmarley,shot).
ingredients(bobmarley,[midorimelonliqueur, jagermeister, goldschlager]).
drink(bobmarley).


alcoholic(bobbyburnscocktail).
category(bobbyburnscocktail,drink).
ingredients(bobbyburnscocktail,[sweetvermouth, scotch, benedictine, lemonpeel]).
drink(bobbyburnscocktail).


alcoholic(boomerang).
category(boomerang,drink).
ingredients(boomerang,[gin, dryvermouth, bitters, maraschinoliqueur, maraschinocherry]).
drink(boomerang).


non_alcoholic(borabora).
category(borabora,cocktail).
ingredients(borabora,[pineapplejuice, passionfruitjuice, lemonjuice, grenadine]).
drink(borabora).


alcoholic(bostonsidecar).
category(bostonsidecar,drink).
ingredients(bostonsidecar,[lightrum, brandy, triplesec, lime]).
drink(bostonsidecar).


alcoholic(bostonsour).
category(bostonsour,drink).
ingredients(bostonsour,[blendedwhiskey, lemon, powderedsugar, eggwhite, lemon, cherry]).
drink(bostonsour).


alcoholic(bourbonsling).
category(bourbonsling,drink).
ingredients(bourbonsling,[sugar, water, lemonjuice, bourbon, lemonpeel]).
drink(bourbonsling).


alcoholic(bourbonsour).
category(bourbonsour,drink).
ingredients(bourbonsour,[bourbon, lemonjuice, sugar, orange, maraschinocherry]).
drink(bourbonsour).


alcoholic(boxcar).
category(boxcar,drink).
ingredients(boxcar,[gin, triplesec, lemonjuice, grenadine, eggwhite]).
drink(boxcar).


alcoholic(brainfart).
category(brainfart,drink).
ingredients(brainfart,[everclear, vodka, mountaindew, surge, lemonjuice, rum]).
drink(brainfart).


alcoholic(brainteaser).
category(brainteaser,shot).
ingredients(brainteaser,[sambuca, erincream, advocaat]).
drink(brainteaser).


alcoholic(bramble).
category(bramble,drink).
ingredients(bramble,[gin, lemonjuice, sugarsyrup, cremedemure]).
drink(bramble).


alcoholic(brandonandwillscokefloat).
category(brandonandwillscokefloat,soda).
ingredients(brandonandwillscokefloat,[vanillaice-cream, coca-cola, bourbon]).
drink(brandonandwillscokefloat).


alcoholic(brandyalexander).
category(brandyalexander,drink).
ingredients(brandyalexander,[brandy, cremedecacao, lightcream, nutmeg]).
drink(brandyalexander).


alcoholic(brandycobbler).
category(brandycobbler,drink).
ingredients(brandycobbler,[sugar, clubsoda, lemon, brandy, maraschinocherry, orange]).
drink(brandycobbler).


alcoholic(brandyflip).
category(brandyflip,drink).
ingredients(brandyflip,[brandy, egg, sugar, lightcream, nutmeg]).
drink(brandyflip).


alcoholic(brandysour).
category(brandysour,drink).
ingredients(brandysour,[brandy, lemon, powderedsugar, lemon, cherry]).
drink(brandysour).


alcoholic(bravebullshooter).
category(bravebullshooter,shot).
ingredients(bravebullshooter,[tequila, tabascosauce]).
drink(bravebullshooter).


alcoholic(brucespuce).
category(brucespuce,shot).
ingredients(brucespuce,[grenadine, kahlua, baileyirishcream]).
drink(brucespuce).


alcoholic(bruisedheart).
category(bruisedheart,shot).
ingredients(bruisedheart,[vodka, chambordraspberryliqueur, peachtreeschnapps, cranberryjuice]).
drink(bruisedheart).


alcoholic(bubblegum).
category(bubblegum,shot).
ingredients(bubblegum,[vodka, bananaliqueur, orangejuice, peachschnapps]).
drink(bubblegum).


alcoholic(buccaneer).
category(buccaneer,beer).
ingredients(buccaneer,[corona, bacardilimon]).
drink(buccaneer).


alcoholic(bumblebee1).
category(bumblebee1,shot).
ingredients(bumblebee1,[baileyirishcream, kahlua, sambuca]).
drink(bumblebee1).


alcoholic(caipirinha).
category(caipirinha,drink).
ingredients(caipirinha,[sugar, lime, cachaca]).
drink(caipirinha).


alcoholic(caipirissima).
category(caipirissima,drink).
ingredients(caipirissima,[lime, sugar, whiterum, ice]).
drink(caipirissima).


alcoholic(californialemonade).
category(californialemonade,drink).
ingredients(californialemonade,[blendedwhiskey, lemon, lime, powderedsugar, grenadine, carbonatedwater]).
drink(californialemonade).


alcoholic(californiarootbeer).
category(californiarootbeer,soda).
ingredients(californiarootbeer,[kahlua, galliano, sodawater]).
drink(californiarootbeer).


alcoholic(camparibeer).
category(camparibeer,beer).
ingredients(camparibeer,[lager, campari]).
drink(camparibeer).


alcoholic(caribbeanboilermaker).
category(caribbeanboilermaker,beer).
ingredients(caribbeanboilermaker,[corona, lightrum]).
drink(caribbeanboilermaker).


alcoholic(caribbeanorangeliqueur).
category(caribbeanorangeliqueur,liqueur).
ingredients(caribbeanorangeliqueur,[orange, vodka, sugar]).
drink(caribbeanorangeliqueur).


alcoholic(casino).
category(casino,drink).
ingredients(casino,[gin, maraschinoliqueur, lemonjuice, orangebitters, cherry]).
drink(casino).


alcoholic(casinoroyale).
category(casinoroyale,drink).
ingredients(casinoroyale,[gin, lemonjuice, maraschinoliqueur, orangebitters, eggyolk]).
drink(casinoroyale).


alcoholic(champagnecocktail).
category(champagnecocktail,drink).
ingredients(champagnecocktail,[champagne, sugar, bitters, lemonpeel, cognac]).
drink(champagnecocktail).


alcoholic(cherryrum).
category(cherryrum,drink).
ingredients(cherryrum,[lightrum, cherrybrandy, lightcream]).
drink(cherryrum).


alcoholic(chicagofizz).
category(chicagofizz,drink).
ingredients(chicagofizz,[lightrum, port, lemon, powderedsugar, eggwhite, carbonatedwater]).
drink(chicagofizz).


alcoholic(chocolateblackrussian).
category(chocolateblackrussian,drink).
ingredients(chocolateblackrussian,[kahlua, vodka, chocolateice-cream]).
drink(chocolateblackrussian).


alcoholic(chocolatemilk).
category(chocolatemilk,shot).
ingredients(chocolatemilk,[chocolateliqueur, milk, amaretto]).
drink(chocolatemilk).


alcoholic(citruscoke).
category(citruscoke,soda).
ingredients(citruscoke,[bacardilimon, coca-cola]).
drink(citruscoke).


alcoholic(cityslicker).
category(cityslicker,drink).
ingredients(cityslicker,[brandy, triplesec, lemonjuice]).
drink(cityslicker).


alcoholic(classicold-fashioned).
category(classicold-fashioned,drink).
ingredients(classicold-fashioned,[bitters, water, sugar, bourbon, orange, maraschinocherry]).
drink(classicold-fashioned).


alcoholic(clovecocktail).
category(clovecocktail,drink).
ingredients(clovecocktail,[sweetvermouth, sloegin, wine]).
drink(clovecocktail).


alcoholic(cloverclub).
category(cloverclub,drink).
ingredients(cloverclub,[gin, grenadine, lemon, eggwhite]).
drink(cloverclub).


alcoholic(coffeeliqueur).
category(coffeeliqueur,liqueur).
ingredients(coffeeliqueur,[coffee, vanillaextract, sugar, vodka, water]).
drink(coffeeliqueur).


alcoholic(coffee-vodka).
category(coffee-vodka,liqueur).
ingredients(coffee-vodka,[water, sugar, coffee, vanilla, vodka, caramelcoloring]).
drink(coffee-vodka).


non_alcoholic(cokeanddrops).
category(cokeanddrops,soda).
ingredients(cokeanddrops,[coca-cola, lemonjuice]).
drink(cokeanddrops).


alcoholic(cosmopolitan).
category(cosmopolitan,cocktail).
ingredients(cosmopolitan,[absolutcitron, limejuice, cointreau, cranberryjuice]).
drink(cosmopolitan).


alcoholic(cosmopolitanmartini).
category(cosmopolitanmartini,cocktail).
ingredients(cosmopolitanmartini,[cointreau, vodka, lime, cranberryjuice]).
drink(cosmopolitanmartini).


alcoholic(cranberrycordial).
category(cranberrycordial,liqueur).
ingredients(cranberrycordial,[cranberries, sugar, lightrum]).
drink(cranberrycordial).


non_alcoholic(cranberrypunch).
category(cranberrypunch,drink).
ingredients(cranberrypunch,[cranberryjuice, sugar, pineapplejuice, almondflavoring, gingerale]).
drink(cranberrypunch).


alcoholic(creamsoda).
category(creamsoda,cocktail).
ingredients(creamsoda,[spicedrum, gingerale]).
drink(creamsoda).


alcoholic(cremedementhe).
category(cremedementhe,liqueur).
ingredients(cremedementhe,[sugar, water, grainalcohol, peppermintextract, foodcoloring]).
drink(cremedementhe).


alcoholic(cubalibra).
category(cubalibra,drink).
ingredients(cubalibra,[darkrum, lime, coca-cola, ice]).
drink(cubalibra).


alcoholic(cubalibre).
category(cubalibre,drink).
ingredients(cubalibre,[lightrum, lime, coca-cola]).
drink(cubalibre).


alcoholic(daiquiri).
category(daiquiri,drink).
ingredients(daiquiri,[lightrum, lime, powderedsugar]).
drink(daiquiri).


alcoholic(damnedifyoudo).
category(damnedifyoudo,shot).
ingredients(damnedifyoudo,[whiskey, hotdamn]).
drink(damnedifyoudo).


alcoholic(darkcaipirinha).
category(darkcaipirinha,cocktail).
ingredients(darkcaipirinha,[demerarasugar, lime, cachaca]).
drink(darkcaipirinha).


alcoholic(darkandstormy).
category(darkandstormy,drink).
ingredients(darkandstormy,[darkrum, gingerbeer]).
drink(darkandstormy).


alcoholic(darkwoodsling).
category(darkwoodsling,soda).
ingredients(darkwoodsling,[cherryheering, sodawater, orangejuice, ice]).
drink(darkwoodsling).


alcoholic(derby).
category(derby,drink).
ingredients(derby,[gin, peachbitters, mint]).
drink(derby).


alcoholic(diesel).
category(diesel,beer).
ingredients(diesel,[lager, cider, blackcurrantcordial]).
drink(diesel).


alcoholic(dirtymartini).
category(dirtymartini,cocktail).
ingredients(dirtymartini,[vodka, dryvermouth, olivebrine, lemon, olive]).
drink(dirtymartini).


alcoholic(dirtynipple).
category(dirtynipple,shot).
ingredients(dirtynipple,[kahlua, sambuca, baileyirishcream]).
drink(dirtynipple).


alcoholic(downshift).
category(downshift,drink).
ingredients(downshift,[fruitpunch, sprite, tequila, unk]).
drink(downshift).


alcoholic(dragonfly).
category(dragonfly,drink).
ingredients(dragonfly,[gin, gingerale, lime]).
drink(dragonfly).


alcoholic(dryrobroy).
category(dryrobroy,drink).
ingredients(dryrobroy,[scotch, dryvermouth, lemonpeel]).
drink(dryrobroy).


alcoholic(dubonnetcocktail).
category(dubonnetcocktail,drink).
ingredients(dubonnetcocktail,[dubonnetrouge, gin, bitters, lemonpeel]).
drink(dubonnetcocktail).


alcoholic(duchampspunch).
category(duchampspunch,cocktail).
ingredients(duchampspunch,[pisco, limejuice, pineapplesyrup, st.germain, angosturabitters, pepper, lavender]).
drink(duchampspunch).


alcoholic(englishhighball).
category(englishhighball,drink).
ingredients(englishhighball,[brandy, gin, sweetvermouth, carbonatedwater, lemonpeel]).
drink(englishhighball).


alcoholic(englishrosecocktail).
category(englishrosecocktail,drink).
ingredients(englishrosecocktail,[apricotbrandy, gin, dryvermouth, grenadine, lemonjuice, cherry]).
drink(englishrosecocktail).


alcoholic(espressomartini).
category(espressomartini,cocktail).
ingredients(espressomartini,[vodka, kahlua, sugarsyrup]).
drink(espressomartini).


alcoholic(fahrenheit5000).
category(fahrenheit5000,shot).
ingredients(fahrenheit5000,[firewater, absolutpeppar, tabascosauce]).
drink(fahrenheit5000).


alcoholic(flamingdrpepper).
category(flamingdrpepper,shot).
ingredients(flamingdrpepper,[amaretto, vodka, unk, dr.pepper, beer]).
drink(flamingdrpepper).


alcoholic(flaminglamborghini).
category(flaminglamborghini,cocktail).
ingredients(flaminglamborghini,[kahlua, sambuca, bluecuracao, baileyirishcream]).
drink(flaminglamborghini).


alcoholic(flandersflake-out).
category(flandersflake-out,drink).
ingredients(flandersflake-out,[sambuca, sarsaparilla]).
drink(flandersflake-out).


alcoholic(flyingdutchman).
category(flyingdutchman,drink).
ingredients(flyingdutchman,[gin, triplesec]).
drink(flyingdutchman).


alcoholic(flyingscotchman).
category(flyingscotchman,drink).
ingredients(flyingscotchman,[scotch, sweetvermouth, bitters, sugarsyrup]).
drink(flyingscotchman).


alcoholic(foxylady).
category(foxylady,drink).
ingredients(foxylady,[amaretto, cremedecacao, lightcream]).
drink(foxylady).


alcoholic(freddykruger).
category(freddykruger,shot).
ingredients(freddykruger,[jagermeister, sambuca, vodka]).
drink(freddykruger).


alcoholic(french75).
category(french75,drink).
ingredients(french75,[gin, sugar, lemonjuice, champagne, orange, maraschinocherry]).
drink(french75).


alcoholic(french75).
category(french75,drink).
ingredients(french75,[gin, sugar, lemonjuice, champagne, orange, maraschinocherry]).
drink(french75).


alcoholic(frenchconnection).
category(frenchconnection,drink).
ingredients(frenchconnection,[cognac, amaretto]).
drink(frenchconnection).


alcoholic(frenchmartini).
category(frenchmartini,cocktail).
ingredients(frenchmartini,[vodka, raspberryliqueur, pineapplejuice]).
drink(frenchmartini).


alcoholic(friscosour).
category(friscosour,drink).
ingredients(friscosour,[blendedwhiskey, benedictine, lemon, lime, lemon, lime]).
drink(friscosour).


alcoholic(frozendaiquiri).
category(frozendaiquiri,drink).
ingredients(frozendaiquiri,[lightrum, triplesec, limejuice, sugar, cherry, ice]).
drink(frozendaiquiri).


alcoholic(frozenmintdaiquiri).
category(frozenmintdaiquiri,drink).
ingredients(frozenmintdaiquiri,[lightrum, limejuice, mint, sugar]).
drink(frozenmintdaiquiri).


alcoholic(frozenpineappledaiquiri).
category(frozenpineappledaiquiri,drink).
ingredients(frozenpineappledaiquiri,[lightrum, pineapple, limejuice, sugar]).
drink(frozenpineappledaiquiri).


alcoholic(gagliardo).
category(gagliardo,cocktail).
ingredients(gagliardo,[peachvodka, lemonjuice, galliano, sirupofroses]).
drink(gagliardo).


alcoholic(gentlemansclub).
category(gentlemansclub,drink).
ingredients(gentlemansclub,[gin, brandy, sweetvermouth, clubsoda]).
drink(gentlemansclub).


alcoholic(gideonsgreendinosaur).
category(gideonsgreendinosaur,drink).
ingredients(gideonsgreendinosaur,[darkrum, vodka, triplesec, tequila, melonliqueur, mountaindew]).
drink(gideonsgreendinosaur).


alcoholic(ginandtonic).
category(ginandtonic,drink).
ingredients(ginandtonic,[gin, tonicwater, lime]).
drink(ginandtonic).


alcoholic(gincooler).
category(gincooler,drink).
ingredients(gincooler,[gin, carbonatedwater, powderedsugar, orangespiral, lemonpeel]).
drink(gincooler).


alcoholic(gindaisy).
category(gindaisy,drink).
ingredients(gindaisy,[gin, lemonjuice, sugar, grenadine, maraschinocherry, orange]).
drink(gindaisy).


alcoholic(ginfizz).
category(ginfizz,drink).
ingredients(ginfizz,[gin, lemon, powderedsugar, carbonatedwater]).
drink(ginfizz).


alcoholic(ginrickey).
category(ginrickey,cocktail).
ingredients(ginrickey,[gin, grenadine, lemon, sodawater, lime]).
drink(ginrickey).


alcoholic(ginsling).
category(ginsling,drink).
ingredients(ginsling,[gin, lemon, powderedsugar, water, orangepeel]).
drink(ginsling).


alcoholic(ginsmash).
category(ginsmash,drink).
ingredients(ginsmash,[gin, carbonatedwater, sugar, mint, orange, cherry]).
drink(ginsmash).


alcoholic(ginsour).
category(ginsour,drink).
ingredients(ginsour,[gin, lemonjuice, sugar, orange, maraschinocherry]).
drink(ginsour).


alcoholic(ginsquirt).
category(ginsquirt,drink).
ingredients(ginsquirt,[gin, grenadine, powderedsugar, pineapple, strawberries, carbonatedwater]).
drink(ginsquirt).


alcoholic(ginswizzle).
category(ginswizzle,drink).
ingredients(ginswizzle,[limejuice, sugar, gin, bitters, clubsoda]).
drink(ginswizzle).


alcoholic(gintoddy).
category(gintoddy,drink).
ingredients(gintoddy,[gin, water, powderedsugar, lemonpeel]).
drink(gintoddy).


alcoholic(girlfromipanema).
category(girlfromipanema,drink).
ingredients(girlfromipanema,[cachaca, lemonjuice, agavesyrup, champagne]).
drink(girlfromipanema).


alcoholic(godchild).
category(godchild,drink).
ingredients(godchild,[vodka, amaretto, heavycream]).
drink(godchild).


alcoholic(godfather).
category(godfather,drink).
ingredients(godfather,[scotch, amaretto]).
drink(godfather).


alcoholic(godmother).
category(godmother,drink).
ingredients(godmother,[vodka, amaretto]).
drink(godmother).


alcoholic(goldendream).
category(goldendream,drink).
ingredients(goldendream,[galliano, triplesec, orangejuice, cream]).
drink(goldendream).


alcoholic(grandblue).
category(grandblue,drink).
ingredients(grandblue,[maliburum, peachschnapps, bluecuracao, sweetandsour]).
drink(grandblue).


alcoholic(grassskirt).
category(grassskirt,drink).
ingredients(grassskirt,[gin, triplesec, pineapplejuice, grenadine, pineapple]).
drink(grassskirt).


alcoholic(grasshopper).
category(grasshopper,drink).
ingredients(grasshopper,[greencremedementhe, cremedecacao, lightcream]).
drink(grasshopper).


alcoholic(greengoblin).
category(greengoblin,beer).
ingredients(greengoblin,[cider, lager, bluecuracao]).
drink(greengoblin).


alcoholic(grimreaper).
category(grimreaper,drink).
ingredients(grimreaper,[kahlua, unk, grenadine]).
drink(grimreaper).


alcoholic(grizzlybear).
category(grizzlybear,drink).
ingredients(grizzlybear,[amaretto, jagermeister, kahlua, milk]).
drink(grizzlybear).


alcoholic(happyskipper).
category(happyskipper,drink).
ingredients(happyskipper,[spicedrum, gingerale, lime, ice]).
drink(happyskipper).


alcoholic(harveywallbanger).
category(harveywallbanger,drink).
ingredients(harveywallbanger,[vodka, galliano, orangejuice]).
drink(harveywallbanger).


alcoholic(havanacocktail).
category(havanacocktail,drink).
ingredients(havanacocktail,[lightrum, pineapplejuice, lemonjuice]).
drink(havanacocktail).


alcoholic(hawaiiancocktail).
category(hawaiiancocktail,drink).
ingredients(hawaiiancocktail,[gin, triplesec, pineapplejuice]).
drink(hawaiiancocktail).


alcoholic(hemingwayspecial).
category(hemingwayspecial,drink).
ingredients(hemingwayspecial,[rum, grapefruitjuice, maraschinoliqueur, limejuice]).
drink(hemingwayspecial).


alcoholic(highlandflingcocktail).
category(highlandflingcocktail,drink).
ingredients(highlandflingcocktail,[scotch, sweetvermouth, orangebitters, olive]).
drink(highlandflingcocktail).


non_alcoholic(holloweenpunch).
category(holloweenpunch,drink).
ingredients(holloweenpunch,[grapejuice, carbonatedsoftdrink, sherbet, sherbet]).
drink(holloweenpunch).


alcoholic(homemadekahlua).
category(homemadekahlua,liqueur).
ingredients(homemadekahlua,[sugar, cornsyrup, coffee, vanillaextract, water, vodka]).
drink(homemadekahlua).


alcoholic(horsesneck).
category(horsesneck,drink).
ingredients(horsesneck,[lemonpeel, brandy, gingerale, bitters]).
drink(horsesneck).


alcoholic(icepick1).
category(icepick1,drink).
ingredients(icepick1,[vodka, icedtea, lemonjuice]).
drink(icepick1).


alcoholic(imperialcocktail).
category(imperialcocktail,cocktail).
ingredients(imperialcocktail,[limejuice, gin, aperol]).
drink(imperialcocktail).


alcoholic(imperialfizz).
category(imperialfizz,drink).
ingredients(imperialfizz,[lightrum, blendedwhiskey, lemon, powderedsugar, carbonatedwater]).
drink(imperialfizz).


alcoholic(irishcream).
category(irishcream,liqueur).
ingredients(irishcream,[scotch, half-and-half, condensedmilk, coconutsyrup, chocolatesyrup]).
drink(irishcream).


alcoholic(irishcurdlingcow).
category(irishcurdlingcow,cocktail).
ingredients(irishcurdlingcow,[baileyirishcream, bourbon, vodka, orangejuice]).
drink(irishcurdlingcow).


alcoholic(irishrussian).
category(irishrussian,beer).
ingredients(irishrussian,[vodka, kahlua, coca-cola, guinnessstout]).
drink(irishrussian).


alcoholic(irishspring).
category(irishspring,drink).
ingredients(irishspring,[irishwhiskey, peachbrandy, orangejuice, sweetandsour, orange, cherry]).
drink(irishspring).


alcoholic(jackrosecocktail).
category(jackrosecocktail,drink).
ingredients(jackrosecocktail,[applebrandy, grenadine, lime]).
drink(jackrosecocktail).


alcoholic(jackhammer).
category(jackhammer,drink).
ingredients(jackhammer,[jackdaniels, amaretto]).
drink(jackhammer).


alcoholic(jamdonut).
category(jamdonut,shot).
ingredients(jamdonut,[baileysirishcream, chambordraspberryliqueur, sugarsyrup, sugar]).
drink(jamdonut).


alcoholic(japanesefizz).
category(japanesefizz,drink).
ingredients(japanesefizz,[blendedwhiskey, lemon, powderedsugar, port, eggwhite, carbonatedwater]).
drink(japanesefizz).


alcoholic(jelloshots).
category(jelloshots,shot).
ingredients(jelloshots,[vodka, jello, water]).
drink(jelloshots).


alcoholic(jellybean).
category(jellybean,shot).
ingredients(jellybean,[blackberrybrandy, anis]).
drink(jellybean).


alcoholic(jewelofthenile).
category(jewelofthenile,drink).
ingredients(jewelofthenile,[gin, greenchartreuse, yellowchartreuse]).
drink(jewelofthenile).


alcoholic(jitterbug).
category(jitterbug,cocktail).
ingredients(jitterbug,[gin, vodka, grenadine, limejuice, sugar, sugarsyrup, sodawater]).
drink(jitterbug).


alcoholic(johncollins).
category(johncollins,drink).
ingredients(johncollins,[bourbon, lemonjuice, sugar, clubsoda, maraschinocherry, orange]).
drink(johncollins).


alcoholic(kamikaze).
category(kamikaze,drink).
ingredients(kamikaze,[vodka, triplesec, limejuice]).
drink(kamikaze).


alcoholic(karsk).
category(karsk,drink).
ingredients(karsk,[coffee, grainalcohol]).
drink(karsk).


alcoholic(kentuckybandb).
category(kentuckybandb,drink).
ingredients(kentuckybandb,[bourbon, benedictine]).
drink(kentuckybandb).


alcoholic(kentuckycolonel).
category(kentuckycolonel,drink).
ingredients(kentuckycolonel,[bourbon, benedictine, lemonpeel]).
drink(kentuckycolonel).


alcoholic(kir).
category(kir,drink).
ingredients(kir,[cremedecassis, champagne]).
drink(kir).


alcoholic(kirroyale).
category(kirroyale,drink).
ingredients(kirroyale,[cremedecassis, champagne]).
drink(kirroyale).


alcoholic(kissmequick).
category(kissmequick,drink).
ingredients(kissmequick,[cranberryvodka, apfelkorn, schweppesrusschian, applejuice, ice]).
drink(kissmequick).


alcoholic(kiwilemon).
category(kiwilemon,drink).
ingredients(kiwilemon,[kiwiliqueur, bitterlemon, ice]).
drink(kiwilemon).


alcoholic(koolfirstaid).
category(koolfirstaid,shot).
ingredients(koolfirstaid,[unk, kool-aid]).
drink(koolfirstaid).


alcoholic(kool-aidshot).
category(kool-aidshot,shot).
ingredients(kool-aidshot,[vodka, amaretto, sloegin, triplesec, cranberryjuice]).
drink(kool-aidshot).


alcoholic(kool-aidslammer).
category(kool-aidslammer,shot).
ingredients(kool-aidslammer,[kool-aid, vodka]).
drink(kool-aidslammer).


alcoholic(ladylovefizz).
category(ladylovefizz,drink).
ingredients(ladylovefizz,[gin, lightcream, powderedsugar, lemon, eggwhite, carbonatedwater]).
drink(ladylovefizz).


alcoholic(lemondrop).
category(lemondrop,cocktail).
ingredients(lemondrop,[absolutvodka, cointreau, lemon]).
drink(lemondrop).


alcoholic(lemonshot).
category(lemonshot,shot).
ingredients(lemonshot,[galliano, absolutcitron, lemon, sugar, unk]).
drink(lemonshot).


alcoholic(limonacorona).
category(limonacorona,beer).
ingredients(limonacorona,[corona, bacardilimon]).
drink(limonacorona).


alcoholic(lochlomond).
category(lochlomond,drink).
ingredients(lochlomond,[scotch, drambuie, dryvermouth, lemonpeel]).
drink(lochlomond).


alcoholic(londontown).
category(londontown,drink).
ingredients(londontown,[gin, maraschinoliqueur, orangebitters]).
drink(londontown).


alcoholic(lonetreecocktail).
category(lonetreecocktail,drink).
ingredients(lonetreecocktail,[sweetvermouth, gin]).
drink(lonetreecocktail).


alcoholic(lonetreecooler).
category(lonetreecooler,drink).
ingredients(lonetreecooler,[carbonatedwater, gin, dryvermouth, powderedsugar, orangespiral, lemonpeel]).
drink(lonetreecooler).


alcoholic(longislandicedtea).
category(longislandicedtea,drink).
ingredients(longislandicedtea,[vodka, tequila, lightrum, gin, coca-cola, lemonpeel]).
drink(longislandicedtea).


alcoholic(longislandtea).
category(longislandtea,drink).
ingredients(longislandtea,[vodka, lightrum, gin, tequila, lemon, coca-cola]).
drink(longislandtea).


alcoholic(longvodka).
category(longvodka,drink).
ingredients(longvodka,[vodka, lime, angosturabitters, tonicwater, ice]).
drink(longvodka).


alcoholic(lordandlady).
category(lordandlady,drink).
ingredients(lordandlady,[darkrum, tiamaria]).
drink(lordandlady).


alcoholic(lunchbox).
category(lunchbox,beer).
ingredients(lunchbox,[beer, amaretto, orangejuice]).
drink(lunchbox).


alcoholic(maitai).
category(maitai,drink).
ingredients(maitai,[lightrum, orgeatsyrup, triplesec, sweetandsour, cherry]).
drink(maitai).


alcoholic(malibutwister).
category(malibutwister,cocktail).
ingredients(malibutwister,[maliburum, tropicana, cranberryjuice]).
drink(malibutwister).


alcoholic(manhattan).
category(manhattan,cocktail).
ingredients(manhattan,[sweetvermouth, bourbon, angosturabitters, ice, maraschinocherry, orangepeel]).
drink(manhattan).


alcoholic(margarita).
category(margarita,drink).
ingredients(margarita,[tequila, triplesec, limejuice, salt]).
drink(margarita).


alcoholic(martinezcocktail).
category(martinezcocktail,cocktail).
ingredients(martinezcocktail,[gin, dryvermouth, triplesec, orangebitters, cherry]).
drink(martinezcocktail).


alcoholic(martini).
category(martini,cocktail).
ingredients(martini,[gin, dryvermouth, olive]).
drink(martini).


alcoholic(marypickford).
category(marypickford,cocktail).
ingredients(marypickford,[lightrum, pineapplejuice, maraschinoliqueur, grenadine, maraschinocherry]).
drink(marypickford).


alcoholic(miamivice).
category(miamivice,cocktail).
ingredients(miamivice,[unk, pinacoladamix, daiquirimix]).
drink(miamivice).


alcoholic(midnightcowboy).
category(midnightcowboy,drink).
ingredients(midnightcowboy,[bourbon, darkrum, heavycream]).
drink(midnightcowboy).


alcoholic(midnightmanx).
category(midnightmanx,drink).
ingredients(midnightmanx,[kahlua, baileyirishcream, goldschlager, heavycream, coffee]).
drink(midnightmanx).


alcoholic(midnightmint).
category(midnightmint,cocktail).
ingredients(midnightmint,[baileyirishcream, whitecremedementhe, cream]).
drink(midnightmint).


alcoholic(mimosa).
category(mimosa,drink).
ingredients(mimosa,[champagne, orangejuice]).
drink(mimosa).


alcoholic(mississippiplanterspunch).
category(mississippiplanterspunch,drink).
ingredients(mississippiplanterspunch,[brandy, lightrum, bourbon, lemon, powderedsugar, carbonatedwater]).
drink(mississippiplanterspunch).


alcoholic(mojito).
category(mojito,cocktail).
ingredients(mojito,[lightrum, lime, sugar, mint, sodawater]).
drink(mojito).


alcoholic(mojito3).
category(mojito3,cocktail).
ingredients(mojito3,[mint, lemonjuice, darkrum, clubsoda, angosturabitters]).
drink(mojito3).


alcoholic(monkeygland).
category(monkeygland,drink).
ingredients(monkeygland,[gin, benedictine, orangejuice, grenadine]).
drink(monkeygland).


alcoholic(monkeywrench).
category(monkeywrench,drink).
ingredients(monkeywrench,[lightrum, grapefruitjuice, bitters]).
drink(monkeywrench).


alcoholic(moranguito).
category(moranguito,shot).
ingredients(moranguito,[absinthe, tequila, grenadine]).
drink(moranguito).


alcoholic(moscowmule).
category(moscowmule,drink).
ingredients(moscowmule,[vodka, limejuice, gingerale]).
drink(moscowmule).


alcoholic(mothersmilk).
category(mothersmilk,shot).
ingredients(mothersmilk,[goldschlager, butterscotchschnapps, milk]).
drink(mothersmilk).


alcoholic(mudslinger).
category(mudslinger,drink).
ingredients(mudslinger,[southerncomfort, orangejuice, pepsicola]).
drink(mudslinger).


alcoholic(mulledwine).
category(mulledwine,drink).
ingredients(mulledwine,[water, sugar, cloves, cinnamon, lemonpeel, redwine, brandy]).
drink(mulledwine).


alcoholic(nationalaquarium).
category(nationalaquarium,drink).
ingredients(nationalaquarium,[rum, vodka, gin, bluecuracao, sourmix, lemon-limesoda]).
drink(nationalaquarium).


alcoholic(negroni).
category(negroni,drink).
ingredients(negroni,[gin, campari, sweetvermouth]).
drink(negroni).


alcoholic(newyorklemonade).
category(newyorklemonade,cocktail).
ingredients(newyorklemonade,[absolutcitron, grandmarnier, lemonjuice, clubsoda]).
drink(newyorklemonade).


alcoholic(newyorksour).
category(newyorksour,drink).
ingredients(newyorksour,[blendedwhiskey, lemon, sugar, redwine, lemon, cherry]).
drink(newyorksour).


alcoholic(oldfashioned).
category(oldfashioned,cocktail).
ingredients(oldfashioned,[bourbon, angosturabitters, sugar, water]).
drink(oldfashioned).


alcoholic(orangecrush).
category(orangecrush,shot).
ingredients(orangecrush,[vodka, triplesec, orangejuice]).
drink(orangecrush).


alcoholic(orangeoasis).
category(orangeoasis,drink).
ingredients(orangeoasis,[cherrybrandy, gin, orangejuice, gingerale]).
drink(orangeoasis).


alcoholic(orangepush-up).
category(orangepush-up,drink).
ingredients(orangepush-up,[spicedrum, grenadine, orangejuice, sourmix]).
drink(orangepush-up).


non_alcoholic(orangeade).
category(orangeade,cocktail).
ingredients(orangeade,[lemonjuice, orangejuice, sugarsyrup, sodawater]).
drink(orangeade).


alcoholic(oreomudslide).
category(oreomudslide,drink).
ingredients(oreomudslide,[vodka, kahlua, baileyirishcream, vanillaice-cream, oreocookie]).
drink(oreomudslide).


alcoholic(orgasm).
category(orgasm,drink).
ingredients(orgasm,[cremedecacao, amaretto, triplesec, vodka, lightcream]).
drink(orgasm).


alcoholic(owensgrandmothersrevenge).
category(owensgrandmothersrevenge,drink).
ingredients(owensgrandmothersrevenge,[whiskey, beer, lemonade, ice]).
drink(owensgrandmothersrevenge).


alcoholic(paradise).
category(paradise,drink).
ingredients(paradise,[gin, apricotbrandy, orangejuice]).
drink(paradise).


alcoholic(pinacolada).
category(pinacolada,drink).
ingredients(pinacolada,[lightrum, coconutmilk, pineapple]).
drink(pinacolada).


alcoholic(pinkgin).
category(pinkgin,drink).
ingredients(pinkgin,[bitters, gin]).
drink(pinkgin).


alcoholic(pinklady).
category(pinklady,drink).
ingredients(pinklady,[gin, grenadine, lightcream, eggwhite]).
drink(pinklady).


alcoholic(pinkpantypulldowns).
category(pinkpantypulldowns,drink).
ingredients(pinkpantypulldowns,[sprite, pinklemonade, vodka]).
drink(pinkpantypulldowns).


alcoholic(pinkpenocha).
category(pinkpenocha,drink).
ingredients(pinkpenocha,[everclear, vodka, peachschnapps, orangejuice, cranberryjuice]).
drink(pinkpenocha).


alcoholic(piscosour).
category(piscosour,cocktail).
ingredients(piscosour,[pisco, lemonjuice, sugar, ice]).
drink(piscosour).


alcoholic(planterspunch).
category(planterspunch,drink).
ingredients(planterspunch,[darkrum, orgeatsyrup, orangejuice, pineapplejuice]).
drink(planterspunch).


alcoholic(poppedcherry).
category(poppedcherry,drink).
ingredients(poppedcherry,[vodka, cherryliqueur, cranberryjuice, orangejuice]).
drink(poppedcherry).


alcoholic(poppycocktail).
category(poppycocktail,drink).
ingredients(poppycocktail,[gin, cremedecacao]).
drink(poppycocktail).


alcoholic(portandstarboard).
category(portandstarboard,drink).
ingredients(portandstarboard,[grenadine, greencremedementhe]).
drink(portandstarboard).


alcoholic(portwinecocktail).
category(portwinecocktail,drink).
ingredients(portwinecocktail,[port, brandy]).
drink(portwinecocktail).


alcoholic(portwineflip).
category(portwineflip,drink).
ingredients(portwineflip,[port, lightcream, powderedsugar, egg, nutmeg]).
drink(portwineflip).


alcoholic(portoflip).
category(portoflip,drink).
ingredients(portoflip,[brandy, port, eggyolk]).
drink(portoflip).


non_alcoholic(pyschvitaminlight).
category(pyschvitaminlight,drink).
ingredients(pyschvitaminlight,[orangejuice, applejuice, pineapplejuice, ice]).
drink(pyschvitaminlight).


alcoholic(quakerscocktail).
category(quakerscocktail,drink).
ingredients(quakerscocktail,[lightrum, brandy, lemon, raspberrysyrup]).
drink(quakerscocktail).


alcoholic(quarterdeckcocktail).
category(quarterdeckcocktail,drink).
ingredients(quarterdeckcocktail,[lightrum, sherry, lime]).
drink(quarterdeckcocktail).


alcoholic(queenbee).
category(queenbee,drink).
ingredients(queenbee,[coffeebrandy, limevodka, sherry]).
drink(queenbee).


alcoholic(queencharlotte).
category(queencharlotte,drink).
ingredients(queencharlotte,[redwine, grenadine, lemon-limesoda]).
drink(queencharlotte).


alcoholic(queenelizabeth).
category(queenelizabeth,drink).
ingredients(queenelizabeth,[dryvermouth, gin, benedictine]).
drink(queenelizabeth).


alcoholic(quentin).
category(quentin,drink).
ingredients(quentin,[darkrum, kahlua, lightcream, nutmeg]).
drink(quentin).


alcoholic(quickf**k).
category(quickf**k,shot).
ingredients(quickf**k,[kahlua, midorimelonliqueur, baileyirishcream]).
drink(quickf**k).


alcoholic(quick-sand).
category(quick-sand,drink).
ingredients(quick-sand,[blacksambuca, orangejuice]).
drink(quick-sand).


alcoholic(radioactivelongislandicedtea).
category(radioactivelongislandicedtea,drink).
ingredients(radioactivelongislandicedtea,[rum, vodka, tequila, gin, triplesec, chambordraspberryliqueur, midorimelonliqueur, maliburum]).
drink(radioactivelongislandicedtea).


alcoholic(radler).
category(radler,drink).
ingredients(radler,[beer, unk]).
drink(radler).


non_alcoholic(railsplitter).
category(railsplitter,cocktail).
ingredients(railsplitter,[sugarsyrup, lemonjuice, gingerale]).
drink(railsplitter).


alcoholic(redsnapper).
category(redsnapper,shot).
ingredients(redsnapper,[crownroyal, amaretto, cranberryjuice]).
drink(redsnapper).


alcoholic(rose).
category(rose,drink).
ingredients(rose,[dryvermouth, gin, apricotbrandy, lemonjuice, grenadine, powderedsugar]).
drink(rose).


alcoholic(royalbitch).
category(royalbitch,shot).
ingredients(royalbitch,[frangelico, crownroyal]).
drink(royalbitch).


alcoholic(royalfizz).
category(royalfizz,drink).
ingredients(royalfizz,[gin, sweetandsour, egg, coca-cola]).
drink(royalfizz).


alcoholic(royalflush).
category(royalflush,shot).
ingredients(royalflush,[crownroyal, peachschnapps, chambordraspberryliqueur, cranberryjuice]).
drink(royalflush).


alcoholic(royalginfizz).
category(royalginfizz,drink).
ingredients(royalginfizz,[gin, lemon, powderedsugar, egg, carbonatedwater]).
drink(royalginfizz).


alcoholic(rubytuesday).
category(rubytuesday,cocktail).
ingredients(rubytuesday,[gin, cranberryjuice, grenadine]).
drink(rubytuesday).


alcoholic(rumcobbler).
category(rumcobbler,drink).
ingredients(rumcobbler,[sugar, clubsoda, lemon, darkrum, maraschinocherry, orange]).
drink(rumcobbler).


alcoholic(rumcooler).
category(rumcooler,drink).
ingredients(rumcooler,[rum, lemon-limesoda, lemon]).
drink(rumcooler).


alcoholic(rummilkpunch).
category(rummilkpunch,drink).
ingredients(rummilkpunch,[lightrum, milk, powderedsugar, nutmeg]).
drink(rummilkpunch).


alcoholic(rumold-fashioned).
category(rumold-fashioned,drink).
ingredients(rumold-fashioned,[lightrum, unk, powderedsugar, bitters, water, limepeel]).
drink(rumold-fashioned).


alcoholic(rumpunch).
category(rumpunch,drink).
ingredients(rumpunch,[rum, gingerale, fruitpunch, orangejuice, ice]).
drink(rumpunch).


alcoholic(rumrunner).
category(rumrunner,drink).
ingredients(rumrunner,[maliburum, blackberrybrandy, orangejuice, pineapplejuice, cranberryjuice]).
drink(rumrunner).


alcoholic(rumscrewdriver).
category(rumscrewdriver,drink).
ingredients(rumscrewdriver,[lightrum, orangejuice]).
drink(rumscrewdriver).


alcoholic(rumsour).
category(rumsour,drink).
ingredients(rumsour,[lightrum, lemonjuice, sugar, orange, maraschinocherry]).
drink(rumsour).


alcoholic(rumtoddy).
category(rumtoddy,drink).
ingredients(rumtoddy,[rum, powderedsugar, lemonpeel, water]).
drink(rumtoddy).


alcoholic(russianspringpunch).
category(russianspringpunch,drink).
ingredients(russianspringpunch,[vodka, cremedecassis, sugarsyrup, lemonjuice]).
drink(russianspringpunch).


alcoholic(rustynail).
category(rustynail,drink).
ingredients(rustynail,[scotch, drambuie, lemonpeel]).
drink(rustynail).


alcoholic(saltydog).
category(saltydog,drink).
ingredients(saltydog,[grapefruitjuice, gin, salt]).
drink(saltydog).


alcoholic(sanfrancisco).
category(sanfrancisco,drink).
ingredients(sanfrancisco,[vodka, cremedebanane, grenadine, orangejuice]).
drink(sanfrancisco).


alcoholic(sangria1).
category(sangria1,drink).
ingredients(sangria1,[redwine, sugar, orangejuice, lemonjuice, cloves, cinnamon]).
drink(sangria1).


alcoholic(sangria-theworldsbest).
category(sangria-theworldsbest,drink).
ingredients(sangria-theworldsbest,[redwine, sugar, lemon, orange, apple, brandy, sodawater]).
drink(sangria-theworldsbest).


alcoholic(sazerac).
category(sazerac,drink).
ingredients(sazerac,[ricard, sugar, peychaudbitters, water, bourbon, lemonpeel]).
drink(sazerac).


alcoholic(scooter).
category(scooter,drink).
ingredients(scooter,[brandy, amaretto, lightcream]).
drink(scooter).


alcoholic(scotchcobbler).
category(scotchcobbler,drink).
ingredients(scotchcobbler,[scotch, brandy, curacao, orange, mint]).
drink(scotchcobbler).


alcoholic(scotchsour).
category(scotchsour,drink).
ingredients(scotchsour,[scotch, lime, powderedsugar, lemon, cherry]).
drink(scotchsour).


alcoholic(scottishhighlandliqueur).
category(scottishhighlandliqueur,liqueur).
ingredients(scottishhighlandliqueur,[johnniewalker, honey, angelicaroot, fennelseeds, lemonpeel]).
drink(scottishhighlandliqueur).


alcoholic(screamingorgasm).
category(screamingorgasm,drink).
ingredients(screamingorgasm,[vodka, baileyirishcream, kahlua]).
drink(screamingorgasm).


alcoholic(screwdriver).
category(screwdriver,drink).
ingredients(screwdriver,[vodka, orangejuice]).
drink(screwdriver).


alcoholic(seabreeze).
category(seabreeze,drink).
ingredients(seabreeze,[vodka, cranberryjuice, grapefruitjuice]).
drink(seabreeze).


alcoholic(sexonthebeach).
category(sexonthebeach,drink).
ingredients(sexonthebeach,[vodka, peachschnapps, cranberryjuice, grapefruitjuice]).
drink(sexonthebeach).


alcoholic(shanghaicocktail).
category(shanghaicocktail,drink).
ingredients(shanghaicocktail,[lightrum, anisette, grenadine, lemon]).
drink(shanghaicocktail).


alcoholic(sharkattack).
category(sharkattack,cocktail).
ingredients(sharkattack,[lemonade, water, vodka]).
drink(sharkattack).


alcoholic(sherryeggnog).
category(sherryeggnog,drink).
ingredients(sherryeggnog,[sherry, powderedsugar, egg, milk, nutmeg]).
drink(sherryeggnog).


alcoholic(sherryflip).
category(sherryflip,drink).
ingredients(sherryflip,[sherry, lightcream, powderedsugar, egg, nutmeg]).
drink(sherryflip).


alcoholic(shot-gun).
category(shot-gun,shot).
ingredients(shot-gun,[jimbeam, jackdaniels, wildturkey]).
drink(shot-gun).


alcoholic(sidecar).
category(sidecar,drink).
ingredients(sidecar,[cognac, cointreau, lemonjuice]).
drink(sidecar).


alcoholic(sidecarcocktail).
category(sidecarcocktail,drink).
ingredients(sidecarcocktail,[brandy, triplesec, lemon]).
drink(sidecarcocktail).


alcoholic(singaporesling).
category(singaporesling,drink).
ingredients(singaporesling,[cherrybrandy, grenadine, gin, sweetandsour, carbonatedwater, cherry]).
drink(singaporesling).


alcoholic(sloegincocktail).
category(sloegincocktail,drink).
ingredients(sloegincocktail,[sloegin, dryvermouth, orangebitters]).
drink(sloegincocktail).


alcoholic(smut).
category(smut,drink).
ingredients(smut,[redwine, peachschnapps, pepsicola, orangejuice]).
drink(smut).


alcoholic(snakebiteuk).
category(snakebiteuk,beer).
ingredients(snakebiteuk,[lager, cider]).
drink(snakebiteuk).


alcoholic(snakebiteandblack).
category(snakebiteandblack,beer).
ingredients(snakebiteandblack,[lager, cider, blackcurrantsquash]).
drink(snakebiteandblack).


alcoholic(snowball).
category(snowball,drink).
ingredients(snowball,[advocaat, lemonade, lemon, ice]).
drink(snowball).


alcoholic(solysombra).
category(solysombra,drink).
ingredients(solysombra,[brandy, anisette]).
drink(solysombra).


alcoholic(spaceodyssey).
category(spaceodyssey,drink).
ingredients(spaceodyssey,[unk, maliburum, pineapplejuice, orangejuice, grenadine, cherries]).
drink(spaceodyssey).


non_alcoholic(spicedpeachpunch).
category(spicedpeachpunch,drink).
ingredients(spicedpeachpunch,[peachnectar, orangejuice, brownsugar, cinnamon, cloves, limejuice]).
drink(spicedpeachpunch).


alcoholic(spritz).
category(spritz,drink).
ingredients(spritz,[prosecco, campari, sodawater]).
drink(spritz).


alcoholic(stinger).
category(stinger,drink).
ingredients(stinger,[brandy, whitecremedementhe]).
drink(stinger).


alcoholic(stonesour).
category(stonesour,drink).
ingredients(stonesour,[apricotbrandy, orangejuice, sweetandsour]).
drink(stonesour).


alcoholic(strawberrydaiquiri).
category(strawberrydaiquiri,drink).
ingredients(strawberrydaiquiri,[strawberryschnapps, lightrum, limejuice, powderedsugar, strawberries]).
drink(strawberrydaiquiri).


non_alcoholic(strawberrylemonade).
category(strawberrylemonade,drink).
ingredients(strawberrylemonade,[lemon, sugar, strawberries, water]).
drink(strawberrylemonade).


alcoholic(strawberrymargarita).
category(strawberrymargarita,drink).
ingredients(strawberrymargarita,[strawberryschnapps, tequila, triplesec, lemonjuice, strawberries, salt]).
drink(strawberrymargarita).


alcoholic(surfcitylifesaver).
category(surfcitylifesaver,drink).
ingredients(surfcitylifesaver,[ouzo, baileyirishcream, gin, grandmarnier]).
drink(surfcitylifesaver).


alcoholic(sweetsangria).
category(sweetsangria,drink).
ingredients(sweetsangria,[redwine, sugar, water, apple, orange, lime, lemon, fresca]).
drink(sweetsangria).


alcoholic(tequilafizz).
category(tequilafizz,drink).
ingredients(tequilafizz,[tequila, lemonjuice, grenadine, eggwhite, gingerale]).
drink(tequilafizz).


alcoholic(tequilasour).
category(tequilasour,drink).
ingredients(tequilasour,[tequila, lemon, powderedsugar, lemon, cherry]).
drink(tequilasour).


alcoholic(tequilasunrise).
category(tequilasunrise,cocktail).
ingredients(tequilasunrise,[tequila, orangejuice, grenadine]).
drink(tequilasunrise).


alcoholic(tequilasurprise).
category(tequilasurprise,shot).
ingredients(tequilasurprise,[tequila, tabascosauce]).
drink(tequilasurprise).


alcoholic(texasrattlesnake).
category(texasrattlesnake,shot).
ingredients(texasrattlesnake,[yukonjack, cherrybrandy, southerncomfort, sweetandsour]).
drink(texasrattlesnake).


alcoholic(theevilbluething).
category(theevilbluething,cocktail).
ingredients(theevilbluething,[cremedecacao, bluecuracao, lightrum]).
drink(theevilbluething).


alcoholic(thriller).
category(thriller,drink).
ingredients(thriller,[scotch, wine, orangejuice]).
drink(thriller).


alcoholic(tia-maria).
category(tia-maria,liqueur).
ingredients(tia-maria,[water, brownsugar, coffee, rum, vanillaextract]).
drink(tia-maria).


alcoholic(tomcollins).
category(tomcollins,drink).
ingredients(tomcollins,[gin, lemonjuice, sugar, clubsoda, maraschinocherry, orange]).
drink(tomcollins).


alcoholic(tommysmargarita).
category(tommysmargarita,drink).
ingredients(tommysmargarita,[tequila, limejuice, agavesyrup]).
drink(tommysmargarita).


alcoholic(turfcocktail).
category(turfcocktail,drink).
ingredients(turfcocktail,[dryvermouth, gin, anis, bitters, orangepeel]).
drink(turfcocktail).


alcoholic(turkeyball).
category(turkeyball,shot).
ingredients(turkeyball,[wildturkey, amaretto, pineapplejuice]).
drink(turkeyball).


alcoholic(tuxedococktail).
category(tuxedococktail,drink).
ingredients(tuxedococktail,[dryvermouth, gin, maraschinoliqueur, anis, orangebitters, cherry]).
drink(tuxedococktail).


alcoholic(valenciacocktail).
category(valenciacocktail,drink).
ingredients(valenciacocktail,[apricotbrandy, orangejuice, orangebitters]).
drink(valenciacocktail).


alcoholic(vampiro).
category(vampiro,drink).
ingredients(vampiro,[tequila, tomatojuice, orangejuice, limejuice, sugarsyrup, salt]).
drink(vampiro).


alcoholic(vanvleet).
category(vanvleet,drink).
ingredients(vanvleet,[lightrum, maplesyrup, lemonjuice]).
drink(vanvleet).


alcoholic(vermouthcassis).
category(vermouthcassis,drink).
ingredients(vermouthcassis,[dryvermouth, cremedecassis, carbonatedwater]).
drink(vermouthcassis).


alcoholic(vesper).
category(vesper,cocktail).
ingredients(vesper,[gin, vodka, lilletblanc]).
drink(vesper).


alcoholic(vesuvio).
category(vesuvio,drink).
ingredients(vesuvio,[lightrum, sweetvermouth, lemon, powderedsugar, eggwhite]).
drink(vesuvio).


alcoholic(veteran).
category(veteran,drink).
ingredients(veteran,[darkrum, cherrybrandy]).
drink(veteran).


alcoholic(victor).
category(victor,drink).
ingredients(victor,[gin, sweetvermouth, brandy]).
drink(victor).


alcoholic(victorycollins).
category(victorycollins,drink).
ingredients(victorycollins,[vodka, lemonjuice, grapejuice, powderedsugar, orange]).
drink(victorycollins).


alcoholic(vodkaandtonic).
category(vodkaandtonic,drink).
ingredients(vodkaandtonic,[vodka, tonicwater]).
drink(vodkaandtonic).


alcoholic(vodkamartini).
category(vodkamartini,drink).
ingredients(vodkamartini,[vodka, dryvermouth, olive]).
drink(vodkamartini).


alcoholic(vodkarussian).
category(vodkarussian,drink).
ingredients(vodkarussian,[vodka, schweppesrusschian]).
drink(vodkarussian).


alcoholic(waikikibeachcomber).
category(waikikibeachcomber,drink).
ingredients(waikikibeachcomber,[triplesec, gin, pineapplejuice]).
drink(waikikibeachcomber).


alcoholic(whiskeysour).
category(whiskeysour,drink).
ingredients(whiskeysour,[blendedwhiskey, lemon, powderedsugar, cherry, lemon]).
drink(whiskeysour).


alcoholic(whiskymac).
category(whiskymac,drink).
ingredients(whiskymac,[scotch, wine]).
drink(whiskymac).


alcoholic(whitelady).
category(whitelady,drink).
ingredients(whitelady,[gin, triplesec, lemonjuice]).
drink(whitelady).


alcoholic(whiterussian).
category(whiterussian,drink).
ingredients(whiterussian,[vodka, coffeeliqueur, lightcream]).
drink(whiterussian).


alcoholic(winepunch).
category(winepunch,drink).
ingredients(winepunch,[redwine, lemon, orangejuice, orange, pineapplejuice]).
drink(winepunch).


alcoholic(yellowbird).
category(yellowbird,cocktail).
ingredients(yellowbird,[whiterum, galliano, triplesec, limejuice]).
drink(yellowbird).


alcoholic(zambeer).
category(zambeer,soda).
ingredients(zambeer,[sambuca, rootbeer, ice]).
drink(zambeer).


alcoholic(zenmeister).
category(zenmeister,drink).
ingredients(zenmeister,[jagermeister, rootbeer]).
drink(zenmeister).


alcoholic(ziemesmartiniapfelsaft).
category(ziemesmartiniapfelsaft,drink).
ingredients(ziemesmartiniapfelsaft,[vermouth, applejuice]).
drink(ziemesmartiniapfelsaft).


alcoholic(zimablaster).
category(zimablaster,drink).
ingredients(zimablaster,[zima, chambordraspberryliqueur]).
drink(zimablaster).


alcoholic(zimadorizinger).
category(zimadorizinger,drink).
ingredients(zimadorizinger,[midorimelonliqueur, zima]).
drink(zimadorizinger).


alcoholic(zinger).
category(zinger,soda).
ingredients(zinger,[peachtreeschnapps, surge]).
drink(zinger).


alcoholic(zipperhead).
category(zipperhead,shot).
ingredients(zipperhead,[chambordraspberryliqueur, vodka, sodawater]).
drink(zipperhead).


alcoholic(zippysrevenge).
category(zippysrevenge,cocktail).
ingredients(zippysrevenge,[amaretto, rum, kool-aid]).
drink(zippysrevenge).


alcoholic(zizicoin-coin).
category(zizicoin-coin,drink).
ingredients(zizicoin-coin,[cointreau, lemonjuice, ice, lemon]).
drink(zizicoin-coin).


alcoholic(zoksel).
category(zoksel,soda).
ingredients(zoksel,[beer, rootbeer, lemonade, coca-cola, unk, cremedecassis, lemon]).
drink(zoksel).


alcoholic(zorbatini).
category(zorbatini,cocktail).
ingredients(zorbatini,[vodka, ouzo]).
drink(zorbatini).


