

% ingredients(Drink,Ingredients,Except)
% function to check if a given drink can be done


check_ingredients(Drink,Except,Ingredients):-
    ingredients(Drink,Ingredients),
    check_ingredients_acc(Ingredients,Except).


check_ingredients_acc([],_).


check_ingredients_acc([H|Lst],Except):-
    not_member(H,Except), % the ingredient doesn't have to be in the excepts list
    quantity(H,X),  % get the quantity of H
    X>0,    % check if it is grather than zero
    check_ingredients_acc(Lst,Except).    



% function to get what is missing for a drink
% missing_ingredients(Drink, Missing)

missing_ingredients(Drink,Missing):-
    ingredients(Drink,Ingredients),
    missing_ingredients_acc(Ingredients,Missing,[]).


missing_ingredients_acc([],Missing,Missing).


missing_ingredients_acc([H|Lst],Missing,Acc):-
    quantity(H,X),  % get the quantity of H
    X<1,    % check if it is grather than zero
    append(Acc,[H],NewAcc),
    missing_ingredients_acc(Lst,Missing,NewAcc).


missing_ingredients_acc([_|Lst],Missing,Acc):-
    missing_ingredients_acc(Lst,Missing,Acc).


% meber(Elem,List)

member(Elem,[Elem|_]).
member(Elem,[_|Lst]):-
    member(Elem,Lst).


not_member(_,[]).
not_member(Elem,[H|Lst]):-
    H \= Elem,
    not_member(Elem,Lst).



