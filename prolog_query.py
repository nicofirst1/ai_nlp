import re
from random import randint

import pandas as pd
import unidecode
from pyswip import Prolog


class CocktailProlog():
    """
    Prolog predicates:
        quantity(ingredient,int for quantity)
        category(drink,string for category)
        alcoholic(drink)
        non_alcoholic(drink)
        ingredients(drink,list of ingredients)
    """

    def __init__(self, paths, generate_kb):
        self.prolog = Prolog()
        self.path = paths

        if generate_kb:
            self.generate_kb()

        # read knoledge bases
        self.prolog.consult(paths.recepies_pl)
        self.prolog.consult(paths.kb_pl)

        self.possibilities, self.prolog_mapping = self.get_possibles()

    def answer_question(self, question):
        """
        Answers a question about drinks
        :param question: a list of words
        :return: a dictionary containing the case scenario, the answer and the question,
                can be either of the following:
                    - drink not found (aka no drink): return the name use for the search
                    - category (search for a specific category of drink): answer
                    - non alcoholic (search for a non alcoholic drinks): answer
                    - alcoholic (search for a non drinks): answer
                    - missing ingredients : list of missing ingredients
                    - drink ok : list of ingredients

        """

        question = [self.prologify(elem) for elem in question]
        excluded = []
        drink = question[0]

        if len(question) > 1:
            excluded = question[1:]

        # check if the question is about a category of drink
        if drink in self.possibilities['categories']:

            # if yes get all the drinks with that specific category
            answer = list(self.prolog.query(f"category(X,{drink})"))
            answer = [elem['X'] for elem in answer]
            answer = [self.prolog_mapping.get(elem, elem) for elem in answer]

            # and return them
            return {"case": "categories", "answer": answer, "question": self.unprologify(question, atom=False)}

        # check if the question is about the non/alcoholic nature of the drink
        elif drink in self.possibilities['alcoholic']:

            # if non is in the request then is non alcoholic
            if "non" not in drink.lower():
                # get the corresponding list of drinks
                answer = list(self.prolog.query(f"alcoholic(X)"))
                # and return them
                return {"case": "non alcoholic", "answer": self.unprologify(answer),
                        "question": self.unprologify(question, atom=False)}

            else:
                # do the same for the alcoholic ones

                answer = list(self.prolog.query(f"non_alcoholic(X)"))

                return {"case": "alcoholic", "answer": self.unprologify(answer),
                        "question": self.unprologify(question, atom=False)}

        else:

            # check if the drink is in the kb
            q = list(self.prolog.query(f"drink({drink})"))
            if len(q) == 0:
                # if not return the name of the drink
                return {"case": "no drink", "answer": drink, "question": self.unprologify(question, atom=False)}

            # if we're here then the reques is about a specific dring
            # check if the drink can be made with the current ingredients
            q = list(self.prolog.query(f"check_ingredients({drink},{excluded},X)"))
            # if not
            if len(q) == 0:

                # get the missing ones and return them
                answer = list(self.prolog.query(f"missing_ingredients({drink},X)"))

                return {"case": "missing ingredient", "answer": self.unprologify(answer[0]['X']),
                        "question": self.unprologify(question, atom=False)}

            # else the drink is feaseble
            else:

                return {"case": "fine drink", "answer": self.unprologify(q[0]['X']),
                        "question": self.unprologify(question, atom=False)}

    def unprologify(self, lst, atom=True):

        if atom:
            lst = [elem.value for elem in lst]
        lst = [self.prolog_mapping.get(elem, elem) for elem in lst]
        return lst

    def prologify(self, string):
        """
        Convert a string to a legal value in prolog
        :param string:
        :return:
        """

        string = string.replace("[\"\',. )(&#]", '')
        string = unidecode.unidecode(string)
        string = string.lower()
        string = string.strip()
        string = string.replace(" ", "")

        return string

    def read_recepies(self):
        """
        Read the recepies file and apply some changes to the dataset before passing it as a list
        :return:
        """

        # read the file
        df = pd.read_csv(self.path.recepies)

        # make a list of columns to drop  and drop them
        to_drop = ['strDrinkThumb', 'strGlass',
                   'strVideo', 'strInstructions',
                   'idDrink', 'strIBA',
                   'dateModified']

        # drop all the measures
        for i in range(1, 16):
            to_drop.append(f"strMeasure{i}")

        df = df.drop(columns=to_drop)

        # drop rows where the alcoholic value is other than non/alcoholic
        df = df[df['strAlcoholic'].isin(['Alcoholic', 'Non alcoholic'])]

        # remove some category
        df = df[df['strCategory'] != 'Other/Unknown']
        df = df[df['strCategory'] != 'Cocoa']
        df = df[df['strCategory'] != 'Milk / Float / Shake']
        df = df[df['strCategory'] != 'Coffee / Tea']

        # define a mapping between categories names
        mapping = {'Ordinary Drink': "Drink",
                   'Homemade Liqueur': 'Liqueur',
                   'Punch / Party Drink': "Drink",
                   'Soft Drink / Soda': "Soda", }
        # and substitute
        df = df.replace(mapping)

        # remove quotes from
        replaced = df['strDrink'].str.replace("[\"\',.)(&#]", '')
        # replace accented characted
        replaced = replaced.apply(lambda x: unidecode.unidecode(x) if isinstance(x, str) else x)
        # remove any drink which starts with a letter
        df['strDrink'] = replaced.apply(lambda x: "Unk" if x[0].isdigit() else x)

        # same thing for ingredients
        for i in range(1, 16):
            replaced = df[f"strIngredient{i}"].apply(lambda x: unidecode.unidecode(x) if isinstance(x, str) else x)
            df[f"strIngredient{i}"] = replaced.apply(lambda x: "Unk" if not pd.isna(x) and x[0].isdigit() else x)

        # return a listed version of the dataframe
        return df.to_dict(orient="list")

    def generate_kb(self):
        """
        Given a dataframe add assertion to it
        :return:
        """

        with open(self.path.recepies_pl, 'w') as file:
            file.write(":-style_check(-discontiguous).\n\n")

        df = self.read_recepies()

        # group all ingredients together
        all_ingredients = []
        for i in range(1, 16):
            all_ingredients += df[f"strIngredient{i}"]

        # assign random quantities to the ingredients
        all_ingredients = [elem.lower() for elem in all_ingredients if not pd.isna(elem)]
        all_ingredients = list(set(all_ingredients))
        self.set_random_ingredients(all_ingredients)

        # for every drink
        for idx in range(len(df['strDrink'])):
            drink = df['strDrink'][idx]

            # set drink type

            # if drink is unk skip
            if drink == "Unk": continue

            # remove spaces
            drink = drink.replace(" ", "")
            drink = drink.lower()

            # get type and category
            type = df['strAlcoholic'][idx]
            category = df['strCategory'][idx]
            category = category.lower()

            # get all the ingredients
            ingredients = []

            for j in range(1, 16):
                ingredients.append(df[f"strIngredient{j}"][idx])

            # remove nan and spaces
            ingredients = [elem.replace(" ", "").lower() for elem in ingredients if not pd.isna(elem)]

            # if no ingredients are found skip the drink
            if len(ingredients) == 0: continue

            # set the alcoholic type if necessary
            if type == 'Alcoholic':
                self.write_on_pl(f"alcoholic({drink}).")
            else:
                self.write_on_pl(f"non_alcoholic({drink}).")

                # set the category
            self.write_on_pl(f"category({drink},{category}).")

            ingredients = str(ingredients).replace("'", "")
            self.write_on_pl(f"ingredients({drink},{ingredients}).")
            self.write_on_pl(f"drink({drink}).")

            self.write_on_pl("\n")

    def write_on_pl(self, what):
        with open(self.path.recepies_pl, "a+") as file:
            file.write(what + "\n")

    def set_random_ingredients(self, all_ingredients):
        """
        Set random quantities to each ingredient
        :param all_ingredients: list of ingredients (strings)
        :return: None
        """

        min = 0
        max = 10
        for ingredient in all_ingredients:
            q = randint(min, max)
            ingredient_format = re.sub("[\"\',.)(&# ]", "", ingredient)

            self.write_on_pl(f"quantity({ingredient_format},{q}).")

    def get_possibles(self):
        """
        Get all the possible values of every item in the dataset
        :return: tuple:
            dictionary of all possible items
            dictionary of mapping between prolog format and original one
        """
        df = self.read_recepies()

        drinks = list(set(df['strDrink']))
        alcoholic = list(set(df['strAlcoholic']))
        categories = list(set(df['strCategory']))

        ingredients = []

        for i in range(1, 16):
            ingredients += df[f"strIngredient{i}"]

        ingredients = [elem for elem in ingredients if not pd.isna(elem)]
        ingredients = list(set(ingredients))

        prolog_mapping = {self.prologify(elem): elem for elem in drinks}
        prolog_mapping.update({self.prologify(elem): elem for elem in alcoholic})
        prolog_mapping.update({self.prologify(elem): elem for elem in categories})
        prolog_mapping.update({self.prologify(elem): elem for elem in ingredients})

        drinks = [self.prologify(elem) for elem in drinks]
        alcoholic = [self.prologify(elem) for elem in alcoholic]
        categories = [self.prologify(elem) for elem in categories]
        ingredients = [self.prologify(elem) for elem in ingredients]

        return {'drinks': drinks, 'alcoholic': alcoholic, 'categories': categories,
                'ingredients': ingredients}, prolog_mapping
