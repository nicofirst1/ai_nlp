import os

from gtts import gTTS


class Asnwering:

    def __init__(self, path):
        print("Initializing answering class")
        self.path = path

    def answer(self, intent):
        """
        Get an answer given an intent
        :param intent:
        :return: None
        """

        # from the intent get the case, answer and question
        case = intent['case']
        answer = intent['answer']
        asked_for = intent['question'][0]

        # get the excluded ingredients too
        excluded = []
        if len(intent['question']) > 1:
            excluded = intent['question'][1:]

        # from noe on there are a series of if/else for each possibility

        if case == "categories":

            self.say(f"The avaiable {asked_for} are listed on the screen")
            print(answer)

        elif case == "non alcoholic":
            self.say(f"The avaiable {asked_for} are listed on the screen")
            print(answer)

        elif case == "alcoholic":
            self.say(f"The avaiable {asked_for} are listed on the screen")
            print(answer)

        elif case == "no drink":
            self.say(f"Sorry we don't have any drink named, {asked_for}, would you like something else?")

        elif case == "missing ingredient":
            to_say = self.add_missing("Sorry we're missing", answer)
            to_say += f"to make the {asked_for} drink."
            self.say(to_say)

        elif case == "fine drink":
            to_say = f"Fine choice! A {asked_for}"
            if len(excluded) > 0:
                to_say += ", without "
                to_say = self.add_missing(to_say, excluded)

            to_say += "is on the way!"
            self.say(f"Fine choice! A {asked_for} is on the way!")

        elif case == "not understood":
            to_say = "Sorry I couldn't understand your question"
            self.say(to_say)

    @staticmethod
    def add_missing(to_say, missing):
        """
        Add missing ingredient to a string
        :param to_say: string
        :param missing: list of strings
        :return: string
        """
        for elem in missing:
            to_say += f"{elem}, "

        return to_say

    @staticmethod
    def say(what):
        """
        Say something
        :param what: a string with something to say
        :return: None
        """
        tmp_file = "tmp.mp3"

        tts = gTTS(what, lang='en')

        tts.save(tmp_file)

        os.system(f"mpg123 {tmp_file}")

        os.remove(tmp_file)
