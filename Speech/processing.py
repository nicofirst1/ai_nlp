import json
import logging

from stanfordcorenlp import StanfordCoreNLP


class StanfordNLP:
    def __init__(self, path):

        print("initializing StandfordNLP")

        self.path = path
        self.nlp = StanfordCoreNLP(path.standford_core, quiet=True, logging_level=logging.WARNING)  # ,memory='2g')
        self.props = {
            'annotators': 'ssplit,pos,ner,parse,depparse,dcoref,relation',
            'pipelineLanguage': 'en',
            'outputFormat': 'json'
        }

    def close(self):
        self.nlp.close()

    def get_intent(self, question):
        annotation = json.loads(self.nlp.annotate(question, properties=self.props))

        # getting the sentence
        sentence = annotation['sentences']

        # only single sentences are supported , so get the first one
        # then get the enhanced dependencies
        dependencies = sentence[0]['enhancedPlusPlusDependencies']

        # save the root
        root = dependencies[0]

        # get all the lement which are dempendent on the root and which are obj
        root_dependent = [elem for elem in dependencies if
                          elem['governor'] == root['dependent'] and elem['dep'] == 'dobj']

        # for every of the previous dependencies take everything which is a depependent and a compund
        root_dep_dep = {}
        for dep in root_dependent:
            res = [elem['dependentGloss'] for elem in dependencies if
                   elem['governor'] == dep['dependent'] and elem['dep'] == "compound"]
            root_dep_dep[dep['dependentGloss']] = " ".join(res)

        case = get_dict_with_value(dependencies, 'case', 'dep')

        if case is not None:
            without = get_dict_with_value(dependencies, 'nmod:without', 'dep')
            if without is not None:
                for dep in without:
                    res = [elem['dependentGloss'] for elem in dependencies if
                           elem['governor'] == dep['dependent'] and elem['dep'] == "compound"]
                    root_dep_dep[dep['dependentGloss']] = " ".join(res)

        what = [f"{v} {k}" for k, v in root_dep_dep.items()]

        return what


def get_dict_with_value(list_of_dicts, val, key):
    results = []
    for d in list_of_dicts:
        if d[key] == val:
            results.append(d)

    if len(results) == 0: return None
    return results
