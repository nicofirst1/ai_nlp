import sched
import time

import speech_recognition as sr
from pynput import keyboard


class RecordListener(keyboard.Listener):

    def __init__(self, nlp, answering, prolog):

        print("Initializing RecordListener")
        super(RecordListener, self).__init__(self.on_press, self.on_release)

        # save all the other calsses
        self.nlp = nlp
        self.answering = answering
        self.prolog = prolog

        # boolean flag for key pressed
        self.key_pressed = None

        # mic and recognizer for understanding audio
        self.mic = sr.Microphone()
        self.r = sr.Recognizer()

        # task cheduler for key pression
        self.task = sched.scheduler(time.time, time.sleep)

    def on_press(self, key):
        return True

    def on_release(self, key):
        # set the flag to not itself when the key is released
        if key.char == 'r':
            self.key_pressed = not self.key_pressed
        return True

    def start(self):
        """
        Call both the listener and the recorder threads
        :return:
        """
        # call the super method for starting the listener
        super(RecordListener, self).start()

        print("Press the 'r' key to begin/stop recording")
        # and the scheduler for the recorder function
        self.task.enter(0.1, 1, self.recorder, ())
        self.task.run()

    def recorder(self):
        """Function to record an audio"""

        # if the key has been pressed then
        if self.key_pressed:

            with self.mic as source:
                # adjust audio to noise
                self.r.adjust_for_ambient_noise(source)
                print("Listening for audio")
                # capture voice
                audio = self.r.listen(source)

            # recognize question
            question = self.r.recognize_google(audio)
            print(f"Understood: {question}")

            # analyze the sentence
            intent = self.nlp.get_intent(question)

            print(f"intent: {intent}")
            # if not understood pass
            if len(intent) == 0:
                answer = {'case': 'not understood', 'question': question, 'answer': 0}
            else:
                # get the possible answers
                answer = self.prolog.answer_question(intent)

            print(f"intent: {answer}")

            # speak them
            self.answering.answer(answer)

            self.key_pressed = False

        # Reschedule the recorder function in 100 ms.
        self.task.enter(0.1, 1, self.recorder, ())


def recognize_audio(path2audio):
    # use the audio file as the audio source
    r = sr.Recognizer()
    with sr.AudioFile(path2audio) as source:
        audio = r.record(source)  # read the entire audio file

    text = ""
    # recognize speech using Google Speech Recognition
    try:

        text = r.recognize_google(audio)
    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))

    return text
